package com.choicify.core.enums;

import lombok.Getter;

/**
 * 
 * @author HostDesign24.com
 *
 */
public enum ChoicifyLanguageType {
	EN("en");

	@Getter
	private String langType;

	ChoicifyLanguageType(final String type) {
		this.langType = type;
	}
}
