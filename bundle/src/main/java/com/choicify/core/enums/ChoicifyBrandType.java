package com.choicify.core.enums;

import lombok.Getter;

/**
 * 
 * @author HostDesign24.com
 *
 */
public enum ChoicifyBrandType {
	DM("dm");

	@Getter
	private String brandType;

	ChoicifyBrandType(final String type) {
		this.brandType = type;
	}
}
