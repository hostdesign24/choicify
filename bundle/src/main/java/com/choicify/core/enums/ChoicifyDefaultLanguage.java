package com.choicify.core.enums;

import lombok.Getter;

/**
 * 
 * @author HostDesign24.com
 *
 */
public enum ChoicifyDefaultLanguage {
	DM("en"), ROSSMANN("en"), ICA("en"), VITA("en"), NORAML("en"), SCHWARZKOPF("en");

	@Getter
	private String langType;

	ChoicifyDefaultLanguage(final String type) {
		this.langType = type;
	}
}
