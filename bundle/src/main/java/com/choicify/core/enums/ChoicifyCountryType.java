package com.choicify.core.enums;

import lombok.Getter;

/**
 * 
 * @author HostDesign24.com
 *
 */
public enum ChoicifyCountryType {
	DE("de");

	@Getter
	private String countryType;

	ChoicifyCountryType(final String type) {
		this.countryType = type;
	}
}
