package com.choicify.core.enums;

import lombok.Getter;

/**
 * 
 * @author HostDesign24.com
 *
 */
public enum ChoicifyCategoryType {
	HAIRCOLOR("haircolor"), HAIRSTYLE("hairstyle");

	@Getter
	private String categoryType;

	ChoicifyCategoryType(final String type) {
		this.categoryType = type;
	}
}
