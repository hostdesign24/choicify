package com.choicify.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import lombok.Getter;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Model(adaptables = Resource.class)
public class PackshotModel {

	@Getter
	@Inject
	@Optional
	private String fileReference;

	@PostConstruct
	public void postConstruct() {
		// do nothing
	}
}
