package com.choicify.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.Self;
import lombok.Getter;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Model(adaptables = Resource.class)
public class PageModel {

	@Getter
	@Inject
	@Optional
	@Named("jcr:title")
	private String title;

	@Self
	private Resource resource;

	@PostConstruct
	public void postConstruct() {
		// do nothing
	}

	public String getName() {
		return this.resource.getName();
	}

}
