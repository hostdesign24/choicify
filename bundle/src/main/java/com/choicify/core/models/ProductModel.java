package com.choicify.core.models;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.Self;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Model(adaptables = Resource.class)
public class ProductModel {
	@Getter
	@Inject
	@Optional
	private String gtin;

	@Getter
	@Inject
	@Optional
	private String productPagePath;

	@Getter
	@Inject
	@Optional
	private String productUrl;

	@Getter
	@Inject
	@Optional
	@Named("jcr:title")
	private String title;

	@Getter
	@Inject
	@Optional
	private String brandTitle;

	@ChildResource
	@Named("packshot")
	@Optional
	private PackshotModel packshot;

	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	@Named("choicify")
	private Resource choicifyResource;

	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	@Named("choicify/colorationPaths")
	private List<Resource> colorationPaths;

	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	@Named("choicify/colorationPaths")
	private List<ColorationModel> colorationPathsList;

	@Getter
	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	@Named("choicify/filters")
	private Resource filterResource;

	@Self
	private Resource resource;

	@Getter
	@Setter
	private String productPageTitle;
	@Getter
	@Setter
	private String productTitle;
	private boolean suggestedRetailsPrice;
	private boolean selectedStoreOnly;
	private Resource productDetailResource;

	@PostConstruct
	public void postConstruct() {
		init();
	}

	private void init() {
		suggestedRetailsPrice = true;
		selectedStoreOnly = true;
		if (choicifyResource != null) {
			productDetailResource = choicifyResource.getChild("productdetails");
		}
		if (productDetailResource != null) {
			ValueMap valueMap = productDetailResource.adaptTo(ValueMap.class);
			if (valueMap != null) {
				if (valueMap.containsKey("suggestedRetailsPrice")) {
					suggestedRetailsPrice = valueMap.get("suggestedRetailsPrice", Boolean.class);
				}
				if (valueMap.containsKey("selectedStoreOnly")) {
					selectedStoreOnly = valueMap.get("selectedStoreOnly", Boolean.class);
				}
			}
		}
		PageModel productPageModel = null;
		Resource productPageResource = this.resource.getParent().getParent().getParent();
		if (productPageResource != null) {
			Resource productPageJcrContent = productPageResource.getChild("jcr:content");
			if (productPageJcrContent != null) {
				productPageModel = productPageJcrContent.adaptTo(PageModel.class);
			}
			if (productPageModel != null) {
				productPageTitle = productPageModel.getTitle();
			}
		}

		productTitle = this.title;
	}

	public PackshotModel getPackshot() {
		return this.packshot;
	}

	public List<Resource> getColorationPaths() {
		return colorationPaths;
	}

	public List<String> getColorCombinations() {
		List<String> colorCombinations = new ArrayList<>();
		if (colorationPathsList != null && !colorationPathsList.isEmpty()) {
			for (ColorationModel colorationModel : colorationPathsList) {
				if (colorationModel != null && colorationModel.getColorCombinations() != null
						&& !colorationModel.getColorCombinations().isEmpty()) {
					colorCombinations.add(StringUtils.join(colorationModel.getColorCombinations(), ","));
				}
			}
		}
		return colorCombinations;
	}

	public boolean getSelectedStoreOnly() {
		return selectedStoreOnly;
	}

	public boolean getSuggestedRetailsPrice() {
		return suggestedRetailsPrice;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProductModel) {
			return StringUtils.equals(((ProductModel) obj).productPagePath, this.productPagePath);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.productPagePath.hashCode();
	}
}
