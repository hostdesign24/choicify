package com.choicify.core.models;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import lombok.Getter;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
		defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ColorationModel {

	@Getter
	@Inject
	private String naturalHairColor;

	@Getter
	@Inject
	private String naturalHairShade;

	@Getter
	@Inject
	private String targetHairColor;

	@Getter
	@Inject
	private String targetHairShade;

	@Getter
	@Inject
	private int productPriority;

	@Getter
	@Inject
	private String colorTile;

	@SlingObject
	private ResourceResolver resourceResolver;

	@Getter
	private List<String> colorCombinations = new ArrayList<>();

	@PostConstruct
	public void postConstruct() {
		TagManager tagManger = resourceResolver.adaptTo(TagManager.class);
		if (tagManger != null) {
			if (StringUtils.isNotBlank(naturalHairColor)) {
				Tag tag = tagManger.resolve(naturalHairColor);
				naturalHairColor = tag.getName();
				colorCombinations.add(naturalHairColor);
			}
			if (StringUtils.isNotBlank(naturalHairShade)) {
				Tag tag = tagManger.resolve(naturalHairShade);
				naturalHairShade = tag.getName();
				colorCombinations.add(naturalHairShade);
			}
			if (StringUtils.isNotBlank(targetHairColor)) {
				Tag tag = tagManger.resolve(targetHairColor);
				targetHairColor = tag.getName();
				colorCombinations.add(targetHairColor);
			}
			if (StringUtils.isNotBlank(targetHairShade)) {
				Tag tag = tagManger.resolve(targetHairShade);
				targetHairShade = tag.getName();
				colorCombinations.add(targetHairShade);
			}
		}
	}
}
