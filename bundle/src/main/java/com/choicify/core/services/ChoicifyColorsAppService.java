package com.choicify.core.services;

import java.util.List;
import javax.jcr.RepositoryException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import com.choicify.core.valueobjects.naturalcolors.NaturalColors;
import com.choicify.core.valueobjects.productsearch.ProductResult;
import com.choicify.core.valueobjects.targetcolors.TargetColors;
import com.choicify.core.valueobjects.targetnaturalcolors.TargetNaturalColors;
import com.day.cq.search.result.Hit;

/**
 * 
 * @author HostDesign24.com
 *
 */
public interface ChoicifyColorsAppService {

	public ProductResult searchProudctPagesByTags(final ResourceResolver resourceResolver,
			final List<String> selectors, final String locale, final String brand, final String country)
			throws RepositoryException;

	public TargetColors searchTargetHairColorTagsOnPages(final ResourceResolver resourceResolver,
			final List<String> selectors, final String locale, final String brand, final String country)
			throws RepositoryException;

	public NaturalColors searchNaturalHairColorTagsOnPages(final ResourceResolver resourceResolver,
			String locale, final String brand, final String country) throws RepositoryException;

	public List<Resource> getResourcesFromSearchResult(List<Hit> searchResult);

	public Resource getValidTagResource(String tagName, ResourceResolver resourceResolver,
			String rootPath);

	public String createPathFromSelector(List<String> selectors);

	public String getNaturalHairColorTagsOnPages(final String locale, final String brand,
			final String country, final ResourceResolver resourceResolver);

	public String getTargetHairColorTagsOnPages(final List<String> selectors, final String locale,
			final String brand, final String country, final ResourceResolver resourceResolver);

	public String getProudctPagesByTags(final List<String> selectors, final String locale,
			final String brand, final String country, final ResourceResolver resourceResolver);

	public List<String> getAllowedsites();

	String getJsonNodeName(List<String> selectors);

	List<String> getCategories();

	List<String> getAvailableCountrys(final ResourceResolver resourceResolver);

	List<String> getChoicifySelectors(List<String> selectors);

	String getChoicifySearchPath();

	List<String> getAvailableLanguages(final ResourceResolver resourceResolver);

	List<String> getChildNodes(String path, ResourceResolver resourceResolver);

	String getTargetNaturalColorTagsOnPages(List<String> selectors, String locale, String brand,
			String country, ResourceResolver resourceResolver);

	TargetNaturalColors searchTargetNaturalColorTagsOnPages(ResourceResolver resourceResolver,
			String locale, String brand, String country) throws RepositoryException;
}
