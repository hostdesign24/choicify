/**
 *
 */
package com.choicify.core.services;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.spi.CachingProvider;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.valueobjects.naturalcolors.NaturalColors;
import com.choicify.core.valueobjects.productsearch.ProductResult;
import com.choicify.core.valueobjects.targetcolors.TargetColors;
import com.choicify.core.valueobjects.targetnaturalcolors.TargetNaturalColors;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Component(immediate = true)
@Service
public class EhCacheService implements CacheService {
	private static final Logger LOG = LoggerFactory.getLogger(EhCacheService.class);

	public static Cache<String, NaturalColors> naturalColorsCache;
	public static Cache<String, TargetColors> targetColorsCache;
	public static Cache<String, TargetNaturalColors> targetNaturalColorsCache;
	public static Cache<String, ProductResult> productsCache;

	public static Cache<String, List> availableCountrysCache;
	public static Cache<String, List> availableLanguagesCache;
	public static Cache<String, List> brandLanguagesCache;

	public static final String NATURAL_COLORS_CACHE = "NaturalColorsCache";
	public static final String TARGET_COLORS_CACHE = "TargetColorsCache";
	public static final String TARGET_NATURAL_COLORS_CACHE = "TargetNaturalColorsCache";
	public static final String PRODUCTS_CACHE = "ProductsCache";

	public static final String AVAILABLE_COUNTRYS_CACHE = "AvailableCountrysCache";
	public static final String AVAILABLE_LANGUAGES_CACHE = "AvailableLanguagesCache";
	public static final String BRAND_LANGUAGES_CACHE = "BrandLanguagesCache";

	private static CachingProvider cachingProvider;
	private static CacheManager cacheManager;

	private static void initializeCache() throws URISyntaxException {
		initCache();

		naturalColorsCache =
				cacheManager.getCache(NATURAL_COLORS_CACHE, String.class, NaturalColors.class);
		targetColorsCache =
				cacheManager.getCache(TARGET_COLORS_CACHE, String.class, TargetColors.class);
		targetNaturalColorsCache =
				cacheManager.getCache(TARGET_NATURAL_COLORS_CACHE, String.class, TargetNaturalColors.class);
		productsCache = cacheManager.getCache(PRODUCTS_CACHE, String.class, ProductResult.class);

		availableCountrysCache =
				cacheManager.getCache(AVAILABLE_COUNTRYS_CACHE, String.class, List.class);
		availableLanguagesCache =
				cacheManager.getCache(AVAILABLE_LANGUAGES_CACHE, String.class, List.class);
		brandLanguagesCache = cacheManager.getCache(BRAND_LANGUAGES_CACHE, String.class, List.class);
	}

	private static void initCache() throws URISyntaxException {
		ClassLoader classLoader = EhCacheService.class.getClassLoader();
		URI config = EhCacheService.class.getResource("/ehcache.xml").toURI();
		cachingProvider = Caching.getCachingProvider(classLoader);
		cacheManager = cachingProvider.getCacheManager(config, classLoader);
	}

	@Activate
	protected void activate(final ComponentContext componentContext) {
		LOG.debug("***** EhCacheService Initializing*****");
		try {
			// Initialize cache
			initializeCache();
		} catch (Exception ex) {
			LOG.error("Exception while EhCacheService Initializing {}", ex);
		}
	}

	@Deactivate
	protected void deactivate(final ComponentContext componentContext) {
		LOG.debug("***** EhCacheService Deactivating*****");
		if (cacheManager != null) {
			cacheManager.close();
		}
		if (cachingProvider != null) {
			cachingProvider.close();
		}
	}

	public static void clearNaturalColorsCache() {
		if (naturalColorsCache != null) {
			naturalColorsCache.clear();
		}
	}

	public static void clearTargetColorsCache() {
		if (targetColorsCache != null) {
			targetColorsCache.clear();
		}
	}

	public static void clearTargetNaturalColorsCache() {
		if (targetNaturalColorsCache != null) {
			targetNaturalColorsCache.clear();
		}
	}

	public static void clearProductsCache() {
		if (productsCache != null) {
			productsCache.clear();
		}
	}

	public static void clearAvalaibleBrandsCache() {
		if (availableCountrysCache != null) {
			availableCountrysCache.clear();
		}

		if (availableLanguagesCache != null) {
			availableLanguagesCache.clear();
		}

		if (brandLanguagesCache != null) {
			brandLanguagesCache.clear();
		}
	}
}
