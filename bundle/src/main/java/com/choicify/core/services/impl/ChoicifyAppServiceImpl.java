package com.choicify.core.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.enums.ChoicifyCategoryType;
import com.choicify.core.services.ChoicifyColorsAppService;
import com.choicify.core.services.EhCacheService;
import com.choicify.core.utils.MissingColorSerializer;
import com.choicify.core.utils.NodeUtils;
import com.choicify.core.valueobjects.fallbackmessage.MissingColor;
import com.choicify.core.valueobjects.naturalcolors.NaturalColors;
import com.choicify.core.valueobjects.productsearch.ProductResult;
import com.choicify.core.valueobjects.productsearch.ProductSearchModel;
import com.choicify.core.valueobjects.targetcolors.TargetColors;
import com.choicify.core.valueobjects.targetnaturalcolors.TargetNaturalColors;
import com.day.cq.search.result.Hit;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Slf4j
@Service(ChoicifyColorsAppService.class)
@Component(metatype = true, immediate = true, label = "Choicify Product Search Service")
public class ChoicifyAppServiceImpl implements ChoicifyColorsAppService {

	@Property(label = "Allowed sites", unbounded = PropertyUnbounded.ARRAY, cardinality = 50,
			description = "Please enter the category url mapping")
	private static final String CHOICIFY_DEFAULT_SITES = "choicify.domain.allowed.sites";

	@Property(label = "Choicify root path of Site", value = "")
	private static final String CHOICIFY_SEARCH_PAGE_PATH = "choicify.search.page.path";

	private String[] allowedsites;

	@Getter
	private String choicifySearchPath;

	@Activate
	@Modified
	protected void activate(final ComponentContext componentContext) {
		Dictionary<?, ?> properties = componentContext.getProperties();
		try {
			this.choicifySearchPath =
					PropertiesUtil.toString(properties.get(CHOICIFY_SEARCH_PAGE_PATH), "");
			this.allowedsites = PropertiesUtil.toStringArray(properties.get(CHOICIFY_DEFAULT_SITES));
		} catch (Exception ex) {
			log.error("Error while reading properties", ex);
		}
	}

	@Override
	public List<String> getAllowedsites() {
		return Arrays.asList(allowedsites);
	}

	@Override
	public List<String> getCategories() {
		return Stream.of(ChoicifyCategoryType.values()).map(ChoicifyCategoryType::getCategoryType)
				.collect(Collectors.toList());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAvailableCountrys(final ResourceResolver resourceResolver) {
		List<String> availableCountrys = new ArrayList<>();
		if (EhCacheService.availableCountrysCache.containsKey("getAvailableCountrys")) {
			availableCountrys =
					(List<String>) EhCacheService.availableCountrysCache.get("getAvailableCountrys");
		} else {
			List<String> brands = getAllowedsites();
			for (String brand : brands) {
				String defaultPath =
						String.format(ChoicifyAppConstants.CHOICIFY_COLORS_DEFAULT_PATH, brand);
				List<String> countrys = getChildNodes(defaultPath, resourceResolver);
				if (!countrys.isEmpty()) {
					availableCountrys.addAll(countrys);
				}
			}
			if (!availableCountrys.isEmpty()) {
				EhCacheService.availableCountrysCache.put("getAvailableCountrys", availableCountrys);
			}
			getAvailableLanguages(resourceResolver);
		}
		return availableCountrys;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAvailableLanguages(final ResourceResolver resourceResolver) {
		List<String> availableLanguages = new ArrayList<>();
		if (EhCacheService.availableLanguagesCache.containsKey("getAvailableLanguages")) {
			availableLanguages =
					(List<String>) EhCacheService.availableLanguagesCache.get("getAvailableLanguages");
		} else {
			List<String> brands = getAllowedsites();
			List<String> brandLanguages = new ArrayList<>();
			try {
				for (String brand : brands) {
					String defaultPath =
							String.format(ChoicifyAppConstants.CHOICIFY_COLORS_DEFAULT_PATH, brand);
					List<String> countrys = getChildNodes(defaultPath, resourceResolver);
					if (!countrys.isEmpty()) {
						for (String country : countrys) {
							String cachedResultsPath =
									String.format(choicifySearchPath, brand, country) + "/cachedResults";
							Node cacheResultsNode = JcrUtils.getNodeIfExists(cachedResultsPath,
									resourceResolver.adaptTo(Session.class));
							if (cacheResultsNode != null && cacheResultsNode.hasProperty("locales")) {
								String existingValue = cacheResultsNode.getProperty("locales").getString();
								brandLanguages = Arrays.asList(StringUtils.split(existingValue, "|"));
								if (brandLanguages != null && !brandLanguages.isEmpty()) {
									availableLanguages.addAll(brandLanguages);
								}
							}
						}
						EhCacheService.brandLanguagesCache.put(brand, brandLanguages);
					}
				}
			} catch (RepositoryException ex) {
				log.error("RepositoryException {}", ex);
			}

			if (!availableLanguages.isEmpty()) {
				availableLanguages = availableLanguages.stream().distinct().collect(Collectors.toList());
				EhCacheService.availableLanguagesCache.put("getAvailableLanguages", availableLanguages);
			}
		}

		return availableLanguages;
	}

	@Override
	public List<String> getChildNodes(final String path, final ResourceResolver resourceResolver) {
		List<String> availableCountrys = new ArrayList<>();

		Resource resource = resourceResolver.getResource(path);
		if (resource != null) {
			Iterator<Resource> childResources = resource.listChildren();
			while (childResources.hasNext()) {
				Resource childResource = childResources.next();
				if (childResource.getName().equals("jcr:content")) {
					continue;
				}
				availableCountrys.add(childResource.getName());
			}
		}
		return availableCountrys;
	}

	@Override
	public String getNaturalHairColorTagsOnPages(final String locale, final String brand,
			final String country, final ResourceResolver resourceResolver) {
		NaturalColors naturalColors = null;

		String cacheKey =
				brand + "_" + locale + "_" + ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR;
		if (EhCacheService.naturalColorsCache.containsKey(cacheKey)) {
			naturalColors = EhCacheService.naturalColorsCache.get(cacheKey);
		} else {
			log.warn("NaturalHairColorTagsOnPages Cachekey not found {}", cacheKey);
			try {
				naturalColors = searchNaturalHairColorTagsOnPages(resourceResolver, locale, brand, country);

				if (naturalColors != null) {
					EhCacheService.naturalColorsCache.put(cacheKey, naturalColors);
				}

			} catch (RepositoryException ex) {
				log.error("LoginException on getNaturalHairColorTagsOnPages : {}", ex);
			}
		}
		return new Gson().toJson(naturalColors);
	}

	@Override
	public NaturalColors searchNaturalHairColorTagsOnPages(final ResourceResolver resourceResolver,
			String locale, final String brand, final String country) throws RepositoryException {
		NaturalColors naturalColors = new NaturalColors();

		String basePath = String.format(this.choicifySearchPath, brand, country);
		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(ChoicifyAppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR).toString();

		Session session = resourceResolver.adaptTo(Session.class);
		Node resultNode = JcrUtils.getNodeIfExists(jsonNodePath, session);
		if (resultNode != null) {
			String propValue = NodeUtils.getStringPropertyFromNode(resultNode, locale);
			if (StringUtils.isNotBlank(propValue)) {
				naturalColors = new Gson().fromJson(propValue, NaturalColors.class);
			}
		} else {
			log.error("Json node doesn't exist {}", jsonNodePath);
		}

		return naturalColors;
	}

	@Override
	public String getTargetHairColorTagsOnPages(final List<String> selectors, final String locale,
			final String brand, final String country, final ResourceResolver resourceResolver) {
		TargetColors targetColors = null;
		List<String> choicifySelectors = getChoicifySelectors(selectors);
		String cacheKey = brand + "_" + locale + "_" + StringUtils.join(choicifySelectors, "_");

		if (EhCacheService.targetColorsCache.containsKey(cacheKey)) {
			targetColors = EhCacheService.targetColorsCache.get(cacheKey);
		} else {
			log.warn("TargetHairColorTagsOnPages Cachekey not found {}", cacheKey);
			try {
				targetColors = searchTargetHairColorTagsOnPages(resourceResolver, choicifySelectors, locale,
						brand, country);

				if (targetColors != null) {
					EhCacheService.targetColorsCache.put(cacheKey, targetColors);
				}

			} catch (RepositoryException ex) {
				log.error("RepositoryException on getTargetHairColorTagsOnPages : {}", ex);
			}
		}
		return new Gson().toJson(targetColors);
	}

	@Override
	public TargetColors searchTargetHairColorTagsOnPages(final ResourceResolver resourceResolver,
			List<String> selectors, final String locale, final String brand, final String country)
			throws RepositoryException {

		TargetColors targetColors = new TargetColors();
		Session session = resourceResolver.adaptTo(Session.class);

		String basePath = String.format(this.choicifySearchPath, brand, country);
		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(ChoicifyAppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR).append("/")
				.append(StringUtils.join(selectors, "_")).toString();

		Node resultNode = JcrUtils.getNodeIfExists(jsonNodePath, session);
		if (resultNode != null) {
			String propValue = NodeUtils.getStringPropertyFromNode(resultNode, locale);
			if (StringUtils.isNotBlank(propValue)) {
				targetColors = new Gson().fromJson(propValue, TargetColors.class);
			}
		} else {
			log.error("Json node doesn't exist {}", jsonNodePath);
			if (!selectors.isEmpty()) {
				selectors.remove(selectors.size() - 1);
				targetColors =
						searchTargetHairColorTagsOnPages(resourceResolver, selectors, locale, brand, country);
			}
		}

		return targetColors;
	}

	@Override
	public String getTargetNaturalColorTagsOnPages(final List<String> selectors, final String locale,
			final String brand, final String country, final ResourceResolver resourceResolver) {
		TargetNaturalColors targetNaturalColors = null;
		List<String> choicifySelectors = getChoicifySelectors(selectors);
		String cacheKey = brand + "_" + locale + "_" + StringUtils.join(choicifySelectors, "_");

		if (EhCacheService.targetNaturalColorsCache.containsKey(cacheKey)) {
			targetNaturalColors = EhCacheService.targetNaturalColorsCache.get(cacheKey);
		} else {
			log.warn("TargetNaturalColorTagsOnPages Cachekey not found {}", cacheKey);
			try {
				targetNaturalColors =
						searchTargetNaturalColorTagsOnPages(resourceResolver, locale, brand, country);

				if (targetNaturalColors != null) {
					EhCacheService.targetNaturalColorsCache.put(cacheKey, targetNaturalColors);
				}

			} catch (RepositoryException ex) {
				log.error("RepositoryException on getTargetNaturalColorTagsOnPages : {}", ex);
			}
		}
		return new Gson().toJson(targetNaturalColors);
	}

	@Override
	public TargetNaturalColors searchTargetNaturalColorTagsOnPages(
			final ResourceResolver resourceResolver, final String locale, final String brand,
			final String country) throws RepositoryException {

		TargetNaturalColors targetNaturalColors = new TargetNaturalColors();
		Session session = resourceResolver.adaptTo(Session.class);

		String basePath = String.format(this.choicifySearchPath, brand, country);
		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(ChoicifyAppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(ChoicifyAppConstants.TARGET_NATURAL_COLORS).toString();

		Node resultNode = JcrUtils.getNodeIfExists(jsonNodePath, session);
		if (resultNode != null) {
			String propValue = NodeUtils.getStringPropertyFromNode(resultNode, locale);
			if (StringUtils.isNotBlank(propValue)) {
				targetNaturalColors = new Gson().fromJson(propValue, TargetNaturalColors.class);
			}
		} else {
			log.error("Json node doesn't exist {}", jsonNodePath);
		}

		return targetNaturalColors;
	}

	public Resource getValidTagResource(String tagName, ResourceResolver resourceResolver,
			String rootPath) {
		Resource resource = resourceResolver.getResource(rootPath);
		if (resource == null) {
			return null;
		}
		Iterator<Resource> tagColors = resource.listChildren();
		while (tagColors.hasNext()) {
			Resource tagColor = tagColors.next();
			if (StringUtils.equals(tagColor.getName(), tagName)) {
				return tagColor;
			}
			Iterator<Resource> tagShades = tagColor.listChildren();
			while (tagShades.hasNext()) {
				Resource tagShade = tagShades.next();
				if (StringUtils.equals(tagShade.getName(), tagName)) {
					return tagShade;
				}
			}

		}
		return null;
	}

	@Override
	public String getProudctPagesByTags(final List<String> selectors, final String locale,
			final String brand, final String country, final ResourceResolver resourceResolver) {
		ProductResult productResult = null;
		List<String> choicifySelectors = getChoicifySelectors(selectors);
		String cacheKey = brand + "_" + locale + "_" + StringUtils.join(choicifySelectors, "_");

		GsonBuilder gson =
				new GsonBuilder().registerTypeAdapter(ProductResult.class, new MissingColorSerializer())
						.excludeFieldsWithoutExposeAnnotation();

		if (EhCacheService.productsCache.containsKey(cacheKey)) {
			productResult = EhCacheService.productsCache.get(cacheKey);
		} else {
			log.warn("ProudctPagesByTags Cachekey not found {}", cacheKey);
			try {
				productResult =
						searchProudctPagesByTags(resourceResolver, choicifySelectors, locale, brand, country);

				if (productResult != null) {
					if (productResult.getProducts() != null && !productResult.getProducts().isEmpty()) {

						Collections.shuffle(productResult.getProducts());
						List<ProductSearchModel> productsPrioirty = productResult.getProducts().stream()
								.sorted(Comparator.comparingInt(ProductSearchModel::getPriority))
								.collect(Collectors.toList());

						productResult.setProducts(productsPrioirty);
					}
					EhCacheService.productsCache.put(cacheKey, productResult);
				}

			} catch (RepositoryException ex) {
				log.error("LoginException on getProudctPagesByTags : {}", ex);
			}
		}

		return gson.create().toJson(productResult);
	}

	@Override
	public ProductResult searchProudctPagesByTags(final ResourceResolver resourceResolver,
			List<String> selectors, final String locale, final String brand, final String country)
			throws RepositoryException {
		ProductResult productResult = new ProductResult();
		Session session = resourceResolver.adaptTo(Session.class);

		String basePath = String.format(this.choicifySearchPath, brand, country);
		String jsonNodePath = new StringBuilder(basePath).append("/")
				.append(ChoicifyAppConstants.CHOICIFY_JSON_RESULTS_NODE).append("/")
				.append(ChoicifyAppConstants.PRODUCTS_SELECTOR).append("/")
				.append(StringUtils.join(selectors, "_")).toString();

		Node resultNode = JcrUtils.getNodeIfExists(jsonNodePath, session);
		if (resultNode != null) {
			String propValue = NodeUtils.getStringPropertyFromNode(resultNode, locale);
			if (StringUtils.isNotBlank(propValue)) {
				GsonBuilder gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
				productResult = gson.create().fromJson(propValue, ProductResult.class);
				if (productResult.getProducts() != null && productResult.getProducts().isEmpty()) {
					String missingColorsValue = NodeUtils.getStringPropertyFromNode(resultNode,
							ChoicifyAppConstants.MISSING_PROP + locale);
					if (StringUtils.isNotBlank(missingColorsValue)) {
						List<MissingColor> missingColors = new ArrayList<>();
						MissingColor missingColor =
								new GsonBuilder().create().fromJson(missingColorsValue, MissingColor.class);
						if (missingColor != null) {
							missingColors.add(missingColor);
							productResult.setMissingColors(missingColors);
						}
					}
				}
			}
		}

		return productResult;
	}

	public List<Resource> getResourcesFromSearchResult(List<Hit> searchResult) {
		List<Resource> result = new ArrayList<>();
		for (Hit hit : searchResult) {
			try {
				result.add(hit.getResource());
			} catch (RepositoryException e) {
				log.warn("while parsing results: {}", e.getMessage());
			}
		}
		return result;
	}

	/**
	 * Create path to store results.
	 * 
	 * @param selectors
	 * @return {@link String}
	 */
	@Override
	public String createPathFromSelector(List<String> selectors) {
		String path = "";
		for (String selector : selectors) {
			if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
					|| StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector,
							ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
				path = path + selector;
			}
		}
		return path;
	}

	@Override
	public String getJsonNodeName(List<String> selectors) {
		String path = StringUtils.EMPTY;
		for (String selector : selectors) {
			if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
					|| StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector,
							ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
				if (StringUtils.isBlank(path)) {
					path = selector;
				} else {
					path = path + "_" + selector;
				}
			}
		}
		return path;
	}

	@Override
	public List<String> getChoicifySelectors(List<String> selectors) {
		List<String> choicifySelectors = new ArrayList<>();
		for (String selector : selectors) {
			if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
					|| StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector,
							ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
				choicifySelectors.add(selector);
			}
		}
		return choicifySelectors;
	}
}
