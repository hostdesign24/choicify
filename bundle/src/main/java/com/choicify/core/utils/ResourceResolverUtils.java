package com.choicify.core.utils;

import java.util.HashMap;
import java.util.Map;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

/**
 * 
 * @author HostDesign24.com
 *
 */
public class ResourceResolverUtils {
	private ResourceResolverUtils() {}

	/**
	 *
	 * @param resourceResolverFactory resourceResolverFactory
	 * @param subservice subservice name for login
	 * @return A ResourceResolver with appropriate permissions to execute the service.
	 * @throws LoginException loginexception
	 */
	public static ResourceResolver getServiceResourceResolver(
			final ResourceResolverFactory resourceResolverFactory, final String subservice)
			throws LoginException {
		final Map<String, Object> param = new HashMap<>();
		param.put(ResourceResolverFactory.SUBSERVICE, subservice);
		return resourceResolverFactory.getServiceResourceResolver(param);
	}

	/**
	 * Close a resource resolver.
	 * 
	 * @param serviceResourceResolver ResourceResolver
	 */
	public static void close(final ResourceResolver resourceResolver) {
		if (resourceResolver != null && resourceResolver.isLive()) {
			resourceResolver.close();
		}
	}
}
