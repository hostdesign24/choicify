package com.choicify.core.utils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import com.choicify.core.enums.ChoicifyBrandType;
import com.choicify.core.enums.ChoicifyCategoryType;
import com.choicify.core.enums.ChoicifyCountryType;
import com.choicify.core.enums.ChoicifyDefaultLanguage;
import com.choicify.core.enums.ChoicifyLanguageType;
import com.choicify.core.services.EhCacheService;

/**
 * 
 * @author HostDesign24.com
 *
 */
public class LangUtils {

	private static final String LANGUAGE_CODE_PATTERN = "(/[a-z]{2}/)";
	private static final String LANGUAGE_CODE_SELECTOR_PATTERN = "(.(de.json|en.json))";

	public static String getLanguageCodeForOldPattern(final List<String> selectors) {
		if (!selectors.isEmpty() && selectors.size() >= 4) {
			return selectors.get(selectors.size() - 1);
		}
		return ChoicifyLanguageType.EN.getLangType();
	}

	public static String getLanguageCodeForOldPattern(final List<String> selectors,
			final String brand) {
		String language = ChoicifyLanguageType.EN.getLangType();
		if (!selectors.isEmpty() && selectors.size() >= 4) {
			language = selectors.get(selectors.size() - 1);
			if (EhCacheService.brandLanguagesCache.containsKey(brand)) {
				List<String> brandLanguagesCache =
						(List<String>) EhCacheService.brandLanguagesCache.get(brand);
				if (!brandLanguagesCache.contains(language)) {
					language = ChoicifyDefaultLanguage.valueOf(StringUtils.upperCase(brand)).getLangType();
				}
			} else {
				language = ChoicifyLanguageType.EN.getLangType();
			}
		}
		return language;
	}

	public static String getBrandForOldPattern(final List<String> selectors) {
		if (!selectors.isEmpty() && selectors.size() >= 3) {
			return selectors.get(selectors.size() - 2);
		}
		return ChoicifyBrandType.DM.getBrandType();
	}

	@SuppressWarnings("unchecked")
	public static String getLanguageCodeFromSelector(final List<String> selectors,
			final String brand) {
		String language = ChoicifyLanguageType.EN.getLangType();
		if (!selectors.isEmpty() && selectors.size() > 4) {
			language = selectors.get(4);

			if (EhCacheService.brandLanguagesCache.containsKey(brand)) {
				List<String> brandLanguagesCache =
						(List<String>) EhCacheService.brandLanguagesCache.get(brand);
				if (!brandLanguagesCache.contains(language)) {
					language = ChoicifyDefaultLanguage.valueOf(StringUtils.upperCase(brand)).getLangType();
				}
			} else {
				language = ChoicifyLanguageType.EN.getLangType();
			}
		}
		return language;
	}

	public static String getLanguageCodeFromSelector(final List<String> selectors) {
		String language = ChoicifyLanguageType.EN.getLangType();
		if (!selectors.isEmpty() && selectors.size() > 4) {
			return language = selectors.get(4);
		}
		return language;
	}

	public static String getMarketFromSelector(final List<String> selectors) {
		if (!selectors.isEmpty()) {
			return selectors.get(1);
		}
		return ChoicifyBrandType.DM.getBrandType();
	}

	public static String getCategoryFromSelector(final List<String> selectors) {
		if (!selectors.isEmpty() && selectors.size() >= 2) {
			return selectors.get(2);
		}
		return ChoicifyCategoryType.HAIRCOLOR.getCategoryType();
	}

	public static String getCountryFromSelector(final List<String> selectors) {
		if (!selectors.isEmpty() && selectors.size() >= 3) {
			return selectors.get(3);
		}
		return ChoicifyCountryType.DE.getCountryType();
	}

	public static String getLanguageCodeFromSelector(SlingHttpServletRequest request) {
		return LangUtils.getLanguageCodeFromSelector(request.getRequestURI());
	}

	public static String getBrandFromSelector(SlingHttpServletRequest request,
			final List<String> availablesites) {
		return LangUtils.getBrandFromSelector(request.getRequestURI(), availablesites);
	}

	public static String getCountryFromSelector(SlingHttpServletRequest request,
			final List<String> availablesites) {
		return LangUtils.getCountryFromSelector(request.getRequestURI(), availablesites);
	}

	public static String getCountryFromSelector(String requestURI, List<String> countrys) {
		String pattenString = "(.(" + StringUtils.join(countrys, "|") + ").)";
		Pattern pattern = Pattern.compile(pattenString);
		Matcher matcher = pattern.matcher(requestURI);
		if (matcher.find()) {
			String countryCode = matcher.group(1);
			if (StringUtils.isNotBlank(countryCode)) {
				countryCode = StringUtils.substringBetween(countryCode, ".");
			}
			return countryCode;
		} else {
			return ChoicifyCountryType.DE.getCountryType();
		}
	}

	public static String getCategoryFromSelector(SlingHttpServletRequest request,
			final List<String> categorys) {
		return LangUtils.getCategoryFromSelector(request.getRequestURI(), categorys);
	}

	public static String getLanguageCode(String path) {

		Pattern pattern = Pattern.compile(LANGUAGE_CODE_PATTERN);
		Matcher matcher = pattern.matcher(path);
		if (matcher.find()) {
			String langCode = matcher.group(1);
			if (StringUtils.isNotBlank(langCode)) {
				langCode = StringUtils.substringBetween(langCode, "/");
			}
			return langCode;
		}
		return StringUtils.EMPTY;
	}

	public static String getLanguageCodeFromSelector(String path) {

		Pattern pattern = Pattern.compile(LANGUAGE_CODE_SELECTOR_PATTERN);
		Matcher matcher = pattern.matcher(path);
		if (matcher.find()) {
			String langCode = matcher.group(1);
			if (StringUtils.isNotBlank(langCode)) {
				langCode = StringUtils.substringBetween(langCode, ".");
			}
			return langCode;
		} else {
			return getLanguageCode(path);
		}
	}

	public static String getBrandFromSelector(String path, final List<String> availablesites) {
		String pattenString = "(.(" + StringUtils.join(availablesites, "|") + ").)";
		Pattern pattern = Pattern.compile(pattenString);
		Matcher matcher = pattern.matcher(path);
		if (matcher.find()) {
			String countryCode = matcher.group(1);
			if (StringUtils.isNotBlank(countryCode)) {
				countryCode = StringUtils.substringBetween(countryCode, ".");
			}
			return countryCode;
		} else {
			return ChoicifyBrandType.DM.getBrandType();
		}
	}

	public static String getCategoryFromSelector(String path, final List<String> categorys) {
		String pattenString = "(.(" + StringUtils.join(categorys, "|") + ").)";
		Pattern pattern = Pattern.compile(pattenString);
		Matcher matcher = pattern.matcher(path);
		if (matcher.find()) {
			String categoryCode = matcher.group(1);
			if (StringUtils.isNotBlank(categoryCode)) {
				categoryCode = StringUtils.substringBetween(categoryCode, ".");
			}
			return categoryCode;
		} else {
			return ChoicifyCategoryType.HAIRCOLOR.getCategoryType();
		}
	}

	public static boolean isCategorySelectorExist(String path, final List<String> categorys) {
		String pattenString = "(.(" + StringUtils.join(categorys, "|") + ").)";
		Pattern pattern = Pattern.compile(pattenString);
		Matcher matcher = pattern.matcher(path);
		return matcher.find();
	}
}
