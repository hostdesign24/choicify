package com.choicify.core.utils;

import java.util.Map;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;
import com.day.cq.commons.jcr.JcrUtil;

/**
 * 
 * @author HostDesign24.com
 *
 */
public final class NodeUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(NodeUtils.class);

	/**
	 * Get the property from the node.
	 *
	 * @param node node
	 * @param propertyName Name of the property
	 * @return property value
	 */
	public static String getStringPropertyFromNode(final Node node, final String propertyName) {

		if (null != node) {
			try {
				if (node.hasProperty(propertyName)) {
					return node.getProperty(propertyName).getString();
				}

			} catch (RepositoryException e) {
				LOGGER.error("RepositoryException in getStringPropertyFromNode()", e);
			}
		}

		return "";
	}

	/**
	 * Get the property from the node if the node under relPath exists.
	 *
	 * @param node node
	 * @param relPath path
	 * @param propertyName Name of the property
	 * @return property value
	 */
	public static String getStringPropertyFromNode(final Node node, final String relPath,
			final String propertyName) {

		if (null != node) {
			try {
				if (node.hasNode(relPath)) {
					return getStringPropertyFromNode(node.getNode(relPath), propertyName);
				}

			} catch (RepositoryException e) {
				LOGGER.error("RepositoryException in getStringPropertyFromNode()", e);
			}
		}

		return "";
	}

	/**
	 * method deletes the node.
	 *
	 * @param node - node to be deleted.
	 *
	 * @throws RepositoryException - RepositoryException
	 */
	public static void deleteNode(final Node node) throws RepositoryException {

		if (null != node) {
			Node parent = node.getParent();
			LOGGER.debug("Node removed under the path [{}]", node.getPath());
			node.remove();
			parent.getSession().save();
		} else {
			LOGGER.debug("Node null not exist in the repository..");
		}

	}

	/**
	 * method return node
	 *
	 * @param resource
	 */
	public static Node getNode(final Resource resource) {

		if (null != resource) {
			return resource.adaptTo(Node.class);
		}

		return null;
	}

	/**
	 * Tokenizes a path and creates a node for each token below the currently focused node. Only the
	 * focus is changed if there is already an existing node.
	 *
	 * @param path - Destination Path
	 * @param initialParent - Initial Parent node
	 * @param nodeType - primary type
	 * @param properties - optional properties, if present, a jcr:content node will be added to each
	 *        page and the given properties will be set
	 * @return Node - JCR Node
	 * @throws RepositoryException - Repository exception
	 */
	public static Node createNodeStructureFromPath(final String path, final Node initialParent,
			final String nodeType, final Map<String, String> properties) throws RepositoryException {
		boolean addStatus = false;
		Node node = null;

		if (StringUtils.isNotBlank(path)) {
			final String[] tokens = path.split("/");
			node = initialParent;

			for (final String title : tokens) {
				if (StringUtils.isNotEmpty(title)) {
					String validName = JcrUtil.createValidName(title);
					if (node.hasNode(title)) {
						node = node.getNode(title);
					} else if (node.hasNode(validName)) {
						node = node.getNode(validName);
					} else {
						addStatus = true;
						node = createNode(node, title, nodeType, properties);
					}
				}
			}

			if (addStatus) {
				initialParent.getSession().save();
			}
		}
		return node;

	}

	/**
	 * Creates a node.
	 *
	 * @param node - the node
	 * @param token - token
	 * @param nodeType - primary type
	 * @param properties - optional properties, if present, a jcr:content node will be added to each
	 *        page and the given properties will be set
	 * @return Node - JCR Node
	 * @throws RepositoryException - Repository exception
	 */
	public static Node createNode(final Node node, final String title, final String nodeType,
			final Map<String, String> properties) throws RepositoryException {
		String validName = JcrUtil.createValidName(title);

		if (null != node) {
			final Node newNode = node.addNode(validName, nodeType);
			if (properties != null) {
				Node jcrNode = newNode.addNode(ChoicifyAppConstants.JCR_CONTENT_NODE,
						ChoicifyAppConstants.CQ_PAGE_CONTENT_NODE);
				jcrNode.setProperty(ChoicifyAppConstants.JCR_TITLE_NODE, title);
				for (Map.Entry<String, String> property : properties.entrySet()) {
					jcrNode.setProperty(property.getKey(), property.getValue());
				}
			}
			LOGGER.trace("createNodeStructureFromPath [{}]", newNode.getPath());

			return newNode;
		}
		return node;

	}

	/**
	 * Method returns the jcr node.
	 *
	 * @param node - Node
	 * @throws RepositoryException -repository exception
	 * @return Node - jcr node
	 */
	public static Node getJcrNode(final Node node) throws RepositoryException {
		Node jcrNode = null;
		if (null != node && node.hasNode(ChoicifyAppConstants.JCR_CONTENT_NODE)) {
			jcrNode = node.getNode(ChoicifyAppConstants.JCR_CONTENT_NODE);
		}
		return jcrNode;
	}

	public static String createValidTagName(String tagName) {
		String validString =
				StringUtils.replaceEach(tagName, new String[] {" ", "&", "ö", "’", "é", "ä", "-", "ü", "‘"},
						new String[] {"", "", "o", "", "e", "a", "", "", ""});
		return JcrUtil.createValidName(validString);
	}
}
