package com.choicify.core.utils;

import java.lang.reflect.Type;
import com.choicify.core.valueobjects.productsearch.ProductResult;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * 
 * @author HostDesign24.com
 *
 */
public class MissingColorSerializer implements JsonSerializer<ProductResult> {

	@Override
	public JsonElement serialize(ProductResult productResult, Type type,
			JsonSerializationContext jsc) {
		JsonObject jObj = (JsonObject) new GsonBuilder().create().toJsonTree(productResult);

		if (productResult.getMissingColors() == null || productResult.getMissingColors().isEmpty()
				|| (productResult.getProducts() != null && !productResult.getProducts().isEmpty())) {
			jObj.remove("missingColors");
		}
		return jObj;
	}
}
