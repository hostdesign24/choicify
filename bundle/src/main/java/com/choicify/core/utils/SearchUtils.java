/**
 *
 */
package com.choicify.core.utils;

import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author HostDesign24.com
 *
 */
public class SearchUtils {
	private static final Logger LOG = LoggerFactory.getLogger(SearchUtils.class);

	private SearchUtils() {}

	public static NodeIterator searchByProperty(final String queryRootPath, final String propertyName,
			final String propertyValue, final ResourceResolver resourceResolver)
			throws RepositoryException {
		final String queryString = String.format(
				"SELECT * FROM [nt:unstructured] AS page WHERE ISDESCENDANTNODE(page, '%s') AND [page].[%s] = '%s'",
				queryRootPath, propertyName, propertyValue);
		Session session = resourceResolver.adaptTo(Session.class);

		return getQueryNodes(session, queryString);
	}

	public static NodeIterator getQueryNodes(final Session session, final String queryString)
			throws RepositoryException {
		Query q = session.getWorkspace().getQueryManager().createQuery(queryString, "JCR-SQL2");
		final NodeIterator resultNodes = q.execute().getNodes();

		if (LOG.isTraceEnabled()) {
			LOG.trace(String.format("query: %s, size: %s", queryString, resultNodes.getSize()));
		}
		return resultNodes;
	}
}
