package com.choicify.core;

/**
 * Choicify Application constants.
 * 
 * @author HostDesign24.com
 *
 */
public class ChoicifyAppConstants {
	/** Json name for natural hair color with shades. **/
	public static final String JSON_NAME_NHC_WITH_NHS = "nhcWithNhs";
	/** String seperator slash. **/
	public static final String STRING_SEPERATOR_SLASH = "/";
	/** String seperator colon. **/
	public static final String STRING_SEPERATOR_COLON = ":";
	/** Query string path name. **/
	public static final String QUERY_STRING_PATH_NAME = "path";
	/** Query String type name. **/
	public static final String QUERY_STRING_TYPE_NAME = "type";
	/** Prop name jcr primary type unstructured. **/
	public static final String JCR_PRIMARY_TYPE_UNSTRUCTURED = "nt:unstructured";
	/** Query String limit name. **/
	public static final String QUERY_STRING_LIMIT_NAME = "p.limit";

	/** Json name for natural hair color. **/
	public static final String NATURAL_HAIR_COLOR_ABBREVIATION = "nhc";
	/** Json name for natural hair shades. **/
	public static final String NATURAL_HAIR_SHADE_ABBREVIATION = "nhs";
	/** Json name for target hair color. **/
	public static final String TARGET_HAIR_COLOR_ABBREVIATION = "thc";
	/** Json name for target hair shades. **/
	public static final String TARGET_HAIR_SHADE_ABBREVIATION = "ths";

	/** Servlet selector natural hair colors. **/
	public static final String NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR = "naturalcolors";
	/** Servlet selector target hair colors. **/
	public static final String TARGET_HAIR_COLORS_AND_SHADES_SELECTOR = "targetcolors";
	public static final String TARGET_NATURAL_COLORS = "targetnaturalcolors";
	/** Servlet selector products. **/
	public static final String PRODUCTS_SELECTOR = "products";
	/** Url extension html. **/
	public static final String HTML_EXTENSION = ".html";
	/** Jcr Property name target hair colors. **/
	public static final String TARGET_HAIR_COLORS_PROP_NAME = "targetHairColor";
	/** Jcr Property name target hair colors. **/
	public static final String TARGET_HAIR_SHADES_PROP_NAME = "targetHairShade";
	/** Jcr Property name natural hair colors. **/
	public static final String NATURAL_HAIR_COLORS_PROP_NAME = "naturalHairColor";
	/** Jcr Property name natural hair colors. **/
	public static final String NATURAL_HAIR_SHADES_PROP_NAME = "naturalHairShade";
	/** Product coloration. **/
	public static final String PRODUCT_COLORATION = "coloration";
	/** Product coloration path. **/
	public static final String PRODUCT_COLORATIONS_PATH = "/choicify/colorationPaths/";
	/** Json name for product filter. **/
	public static final String JSON_NAME_PRODUCT_FILTER = "filters";
	/** Json name for products. **/
	public static final String JSON_NAME_PRODUCTS = "products";
	/** Json name. **/
	public static final String JSON_NAME = "name";
	/** Json key. **/
	public static final String JSON_KEY = "key";
	/** Json options. **/
	public static final String JSON_OPTIONS = "options";
	/** Json tags. **/
	public static final String JSON_TAGS = "tags";
	/** Jcr Primary Type name. **/
	public static final String JCR_PRIMARY_TYPE_NAME = "jcr:primaryType";
	/** jcr node name for products. **/
	public static final String JCR_NODE_NAME_PRODUCT = "product";
	/** Json type. **/
	public static final String JSON_TYPE = "type";
	/** Json select. **/
	public static final String JSON_SELECT = "select";
	/** Servlet Selector Choicify. **/
	public static final String CHOICIFY_SERVLET_SELECTOR = "choicify";
	/** json name for brand page title. **/
	public static final String JSON_NAME_BRAND_PAGE_TITLE = "brandPagetitle";
	/** json name for product page title. **/
	public static final String JSON_NAME_PRODUCT_PAGE_TITLE = "productPageTitle";
	/** Json name for product title. **/
	public static final String JSON_NAME_PRODUCT_TITLE = "productTitle";
	/** json name Product page url. **/
	public static final String JSON_NAME_PRODUCT_PAGE_URL = "productPageUrl";
	/** Json name for product image url. **/
	public static final String JSON_NAME_PRODCUT_IMAGE_URL = "productImageUrl";
	/** json name for suggested retail price. **/
	public static final String JSON_NAME_SUGGESTED_RETAIL_PRICE = "suggestedRetailsPrice";
	/** json name for selected store only. **/
	public static final String JSON_NAME_SELECTED_STORE_ONLY = "selectedStoreOnly";
	/** Choicify AEM dam root path. **/
	public static final String CHOICIFY_DAM_ROOT_PATH = "/content/dam/choicify";
	/** Choicify tag root path. **/
	public static final String CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR = "/etc/tags/choicify/haircolor";
	/** Choicify tag natural colors root path. **/
	public static final String CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR =
			"/etc/tags/choicify/haircolor/naturalhair";
	/** Choicify tag root path target hair color. **/
	public static final String CHOICIFY_TAG_ROOT_PATH_TARGET_HAIR_COLOR =
			"/etc/tags/choicify/haircolor/targethair";
	/** Choicify tag namespace root path. **/
	public static final String CHOICIFY_TAG_ROOT_PATH_NAMESPACE = "/etc/tags/choicify";
	/** Choicify tag filter root path. **/
	public static final String CHOICIFY_TAG_FILTER_ROOT_PATH = "/etc/tags/choicify/filters";
	/** Choicify tag name durability. **/
	public static final String CHOICIFY_TAG_DURABILITY_NAME = "durability";
	/** Choicify tag name permanent. **/
	public static final String CHOICIFY_TAG_PERMANENT_NAME = "permanent";
	/** Choicify tag name tempory. **/
	public static final String CHOICIFY_TAG_TEMPORARY_NAME = "temporary";
	/** Choicify woman head (image) extension .png **/
	public static final String CHOCIFY_ASSET_EXTENSION_PNG = ".png";
	/** Choicify woman head image name. **/
	public static final String CHOCIFY_IMAGE_WOMAN_HEAD_NAME = "Woman";
	/** Choicify folder part name family. **/
	public static final String CHOCIFY_FOLDER_NAME_FAMILY = "Family";
	/** Empty String. **/
	public static final String EMPTY_STRING = "";
	/** Choicify asset (image) extension .jpg **/
	public static final String CHOCIFY_ASSET_EXTENSION_JPG = ".jpg";
	/** natural hair color root path. **/
	public static final String ROOT_PATH_NATURAL_HAIR_COLOR =
			CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR + STRING_SEPERATOR_SLASH + "naturalhair";
	/** target hair color root path. **/
	public static final String ROOT_PATH_TARGET_HAIR_COLOR =
			CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR + STRING_SEPERATOR_SLASH + "targethair";
	/** Choicify root path. **/
	public static final String CHOICIFY_ROOT_PATH = "/choicify";
	/** Gtin name for products. **/
	public static final String CHOICIFY_GTIN_NAME = "gtin";
	/** Durability root path. **/
	public static final String TAG_DURABILITY_ROOT_PATH = "/etc/tags/choicify/filters/durability/";
	public static final String TAG_BRANDS_ROOT_PATH = "/etc/tags/choicify/filters/brands/";
	/** Access Control Allow Credentials. **/
	public static final String ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
	/** Access Control Allow Origin. **/
	public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	/** Access Control Allow Headers. **/
	public static final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
	/** Access Control Allow Methods. **/
	public static final String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
	/** Origin. **/
	public static final String ORIGIN = "Origin";
	/** path for temporal color results . **/
	public static final String CHOICIFY_RESULTS_ROOT_PATH = "/var/choicify/";
	/** color results name in jcr node. **/
	public static final String PROP_NAME_COLOR_RESULTS = "results";
	/** product results name in jcr node. **/
	public static final String PROP_NAME_PRODUCT_RESULTS = "products";
	/** product root path. **/
	public static final String CHOICIFY_PRODUCT_ROOT_PATH = "/content/choicify/";
	/** product lib schwarzkopf root path. **/
	public static final String CHOICIFY_PRODUCT_LIB_ROOT_PATH = "/content/productlib/schwarzkopf";
	/** product result path. **/
	public static final String CHOICIFY_PRODUCT_RESULT_PATH =
			CHOICIFY_RESULTS_ROOT_PATH + "importproducts";
	public static final String CHOICIFY_METADATA_RESULT_PATH =
			CHOICIFY_RESULTS_ROOT_PATH + "metadataimporter";
	public static final String CHOICIFY_HEALTHCHECK_RESULT_PATH =
			CHOICIFY_RESULTS_ROOT_PATH + "healthCheckProducts";

	/** Properties for the jcr:content node. */
	public static final String JCR_CONTENT_NODE = "jcr:content";
	public static final String CQ_PAGE_CONTENT_NODE = "cq:PageContent";
	public static final String JCR_TITLE_NODE = "jcr:title";
	public static final String CQ_PAGE_NODE = "cq:Page";
	public static final String CQ_TEMPLATE_PROPERTY = "cq:template";
	public static final String SLING_RESOURCETYPE = "sling:resourceType";
	public static final String HIDE_IN_NAVIGATION = "hideInNav";
	public static final String SLING_ORDERED_FOLDER = "sling:OrderedFolder";

	/** Resourcetype Constants. */
	public static final String RESOURCETYPE_FOLDER = "choicify/components/page/folderpage";
	public static final String RESOURCETYPE_PRODUCT = "choicify/components/page/productpage";

	/** Template Constants. */
	public static final String TEMPLATE_FOLDER = "/apps/choicify/templates/folderpage";
	public static final String TEMPLATE_PRODUCT = "/apps/choicify/templates/productpage";

	/** Product Constants. */
	public static final String CHOICIFY_PRODUCT_URL = "productUrl";
	public static final String CHOICIFY_PRODUCT_PAGE_PATH = "productPagePath";
	public static final String CHOICIFY_FILE_REFERENCE = "fileReference";

	/** jcr node name for packshot. **/
	public static final String JCR_NODE_NAME_PACKSHOT = "packshot";
	/** Property name searchResults. **/
	public static final String STORED_RESULTS_PROP_NAME_RESULTS = "searchResults";
	/** selector to clean stored caches. **/
	public static final String SELECTOR_CLEAN_CACHES = "cleanCachedResults";
	/** selector to generate cached results. **/
	public static final String SELECTOR_GENERATE_CACHED_RESULTS = "generateCachedResults";
	/** selector one. **/
	public static final String SELECTOR_ONE = "one";
	/** selector two. **/
	public static final String SELECTOR_TWO = "two";
	/** selector THREE. **/
	public static final String SELECTOR_THREE = "three";
	/** selector four. **/
	public static final String SELECTOR_FOUR = "four";
	/** all search selector. **/
	public static final String SELECTOR_ALL_SEARCH = "allSearch";
	/** choicify Service name. **/
	public static final String CHOICIFY_SERVICE = "choicifyService";

	public static final String BRANDS = "brands";
	public static final String BRAND_TITLE = "brandTitle";
	public static final String PRODUCT_PRIORITY = "productPriority";
	public static final String COLOR_TILE = "colorTile";
	public static final String COLORATIONPATHS = "/colorationPaths";
	public static final String COLOR_TILE_LOCATION = "/content/dam/choicify/colortiles/";
	public static final String PERFECT_CAMCODE_TILE_PATH =
			"https://schwarzkopf.de/content/dam/choicify/perfectcam/";

	public static final String CHOICIFY_JSON_RESULTS_NODE = "cachedResults";
	public static final String CHOICIFY_COLORS_DEFAULT_PATH = "/content/choicify/%s/haircolor/";
	public static final String MISSING_PROP = "missingcolor_";
}
