package com.choicify.core.jmx;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.services.EhCacheService;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Component(immediate = true)
@Property(name = "jmx.objectname", value = "com.choicifycolors.jmx:type=Clear EH Cache MBean")
@Service
public class ClearEhCachesMBeanImpl implements ClearEhCachesMBean {
	private static final Logger LOGGER = LoggerFactory.getLogger(ClearEhCachesMBeanImpl.class);

	@Override
	public void clearAllCaches() {
		try {
			LOGGER.debug("clear all eh caches");
			EhCacheService.clearNaturalColorsCache();
			EhCacheService.clearProductsCache();
			EhCacheService.clearTargetColorsCache();
			EhCacheService.clearTargetNaturalColorsCache();
		} catch (Exception ex) {
			LOGGER.error("Exception while cleaning caches {}", ex);
		}
	}

	@Override
	public void clearNaturalColorsCache() {
		try {
			EhCacheService.clearNaturalColorsCache();
		} catch (Exception ex) {
			LOGGER.error("Exception while clearNaturalColorsCache {}", ex);
		}
	}

	@Override
	public void clearTargetColorsCache() {
		try {
			EhCacheService.clearTargetColorsCache();
		} catch (Exception ex) {
			LOGGER.error("Exception while clearTargetColorsCache {}", ex);
		}
	}
  
	@Override
	public void clearProductsCache() {
		try {
			EhCacheService.clearProductsCache();
		} catch (Exception ex) {
			LOGGER.error("Exception while clearProductsCache {}", ex);
		}
	}
}
