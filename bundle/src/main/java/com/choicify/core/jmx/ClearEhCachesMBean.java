package com.choicify.core.jmx;

/**
 * The Interface ClearEhCachesMBean.
 * 
 * @author HostDesign24.com
 *
 */
public interface ClearEhCachesMBean {
	void clearAllCaches();

	public void clearNaturalColorsCache();

	public void clearTargetColorsCache();

	public void clearProductsCache();
}
