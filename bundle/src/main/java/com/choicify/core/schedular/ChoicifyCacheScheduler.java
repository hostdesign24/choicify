package com.choicify.core.schedular;

import java.util.Dictionary;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.jmx.ClearEhCachesMBean;
import com.choicify.core.services.ChoicifyColorsAppService;
import com.choicify.core.utils.ResourceResolverUtils;
import com.day.cq.commons.Externalizer;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Component(immediate = true, metatype = true)
@Service(value = Runnable.class)
@Properties({@Property(name = "scheduler.expression", value = "0 0 1 1/1 * ? *"),
		@Property(name = "scheduler.concurrent", boolValue = false)})
public class ChoicifyCacheScheduler implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(ChoicifyCacheScheduler.class);

	private static final boolean DEF_SCHEDULER_ENABLED = true;
	@Property(label = "Scheduler enabled", description = "Enable or disable store import scheduler",
			boolValue = DEF_SCHEDULER_ENABLED)
	private static final String CONF_SCHEDULER_ENABLED = "scheduler.enabled";

	@Reference
	protected ResourceResolverFactory resourceResolverFactory;

	@Reference
	private ChoicifyColorsAppService choicifyAppService;

	@Reference
	private ClearEhCachesMBean clearEhCacheService;

	@Reference
	protected SlingSettingsService slingSettings;

	private static boolean running = false;
	private boolean enabled;

	@Activate
	@Modified
	public void activate(final ComponentContext componentContext) {
		try {
			Dictionary<?, ?> osgiProperties = componentContext.getProperties();
			this.enabled = PropertiesUtil.toBoolean(osgiProperties.get(CONF_SCHEDULER_ENABLED),
					DEF_SCHEDULER_ENABLED);
		} catch (Exception ex) {
			LOGGER.error("Exception while activating Schedular {}", ex);
		}
	}

	@Deactivate
	public void deactivate(final ComponentContext componentContext) {
		// do nothing
	}

	public void run() {
		long startTimestamp = System.currentTimeMillis();
		try {
			LOGGER.info("ChoicifyCacheScheduler :: started");
			if (running) {
				LOGGER.info("Cache is already warming up --> try again later");
			} else if (!this.enabled) {
				LOGGER.info("ChoicifyCacheScheduler is disabled --> do nothing");
			} else {
				running = true;
				this.warmupCache();
				running = false;
			}
		} catch (Exception ex) {
			running = false;
			LOGGER.error("Exception {}", ex);
		}
		LOGGER.info("ChoicifyCacheScheduler :: finished in {} minutes",
				(((System.currentTimeMillis() - startTimestamp) / 1000) / 60));
	}

	private void warmupCache() {
		ResourceResolver resourceResolver = null;

		try {
			if (this.slingSettings.getRunModes().contains(Externalizer.PUBLISH)) {
				resourceResolver = ResourceResolverUtils.getServiceResourceResolver(
						this.resourceResolverFactory, ChoicifyAppConstants.CHOICIFY_SERVICE);
				clearEhCacheService.clearAllCaches();
				// CacheBuilder.buildcache(resourceResolver, choicifyAppService);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception {}", ex);
		} finally {
			ResourceResolverUtils.close(resourceResolver);
		}
	}

	public static boolean isRunning() {
		return running;
	}
}
