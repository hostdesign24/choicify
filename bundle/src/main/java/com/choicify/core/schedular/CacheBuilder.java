package com.choicify.core.schedular;

import java.util.ArrayList;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.services.ChoicifyColorsAppService;
import com.choicify.core.services.EhCacheService;
import com.choicify.core.valueobjects.naturalcolors.NaturalColors;
import com.choicify.core.valueobjects.targetcolors.TargetColors;
import com.choicify.core.valueobjects.targetnaturalcolors.TargetNaturalColors;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Slf4j
public class CacheBuilder {

	private CacheBuilder() {}

	public static void buildcache(final ResourceResolver resourceResolver,
			final ChoicifyColorsAppService choicifyAppService) throws Exception {

		List<String> brands = choicifyAppService.getAllowedsites();
		for (String brand : brands) {
			log.error("Building cache for {}", brand);
			String defaultPath = String.format(ChoicifyAppConstants.CHOICIFY_COLORS_DEFAULT_PATH, brand);
			List<String> countrys = choicifyAppService.getChildNodes(defaultPath, resourceResolver);
			for (String country : countrys) {

				String basePath = String.format(choicifyAppService.getChoicifySearchPath(), brand, country);
				String cacheResultsPath = new StringBuilder(basePath).append("/")
						.append(ChoicifyAppConstants.CHOICIFY_JSON_RESULTS_NODE).toString();
				Node cacheResultsNode =
						JcrUtils.getNodeIfExists(cacheResultsPath, resourceResolver.adaptTo(Session.class));
				if (cacheResultsNode != null && cacheResultsNode.hasProperty("locales")) {
					String existingValue = cacheResultsNode.getProperty("locales").getString();
					String[] existingLocales = StringUtils.split(existingValue, "|");
					if (existingLocales != null && existingLocales.length > 0) {
						for (String locale : existingLocales) {
							cacheProductResultsAndCached(resourceResolver, choicifyAppService, locale, brand,
									country);
							cacheTargetResultsAndCached(resourceResolver, choicifyAppService, locale, brand,
									country);
							cacheNaturalHairColorTags(resourceResolver, choicifyAppService, locale, brand,
									country);
							cacheTargetNaturalHairColorTags(resourceResolver, choicifyAppService, locale, brand,
									country);
						}
					}
				}
			}
		}
	}

	private static void cacheProductResultsAndCached(final ResourceResolver resourceResolver,
			final ChoicifyColorsAppService choicifyAppService, final String locale, final String brand,
			final String country) throws RepositoryException {
		log.debug("Start: ProductResults");
		//choicifyAppService.buildProductCache(resourceResolver, choicifyAppService, locale, brand, country);
		log.debug("End: ProductResults");
	}

	@SuppressWarnings("unchecked")
	private static void cacheTargetResultsAndCached(final ResourceResolver resourceResolver,
			final ChoicifyColorsAppService choicifyAppService, final String locale, final String brand,
			final String country) {
		log.debug("Start: TargetResults");
		Resource resourceRootNhc = resourceResolver
				.getResource(ChoicifyAppConstants.CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR);
		if (resourceRootNhc != null) {
			List<Resource> resourceNhcs = IteratorUtils.toList(resourceRootNhc.listChildren());
			List<String> selectors = null;
			for (Resource resourceNhc : resourceNhcs) {
				selectors = new ArrayList<>();
				selectors.add(resourceNhc.getName());
				generateTargetResultsAndCache(resourceResolver, choicifyAppService, selectors, locale,
						brand, country);
				List<Resource> resourceNhcNhss = IteratorUtils.toList(resourceNhc.listChildren());
				for (Resource resourceNhcNhs : resourceNhcNhss) {
					selectors = new ArrayList<>();
					selectors.add(resourceNhc.getName());
					selectors.add(resourceNhcNhs.getName());
					generateTargetResultsAndCache(resourceResolver, choicifyAppService, selectors, locale,
							brand, country);
				}
			}
		}
		log.debug("End: TargetResults");
	}

	private static void generateTargetResultsAndCache(final ResourceResolver resourceResolver,
			final ChoicifyColorsAppService choicifyAppService, final List<String> selectors,
			final String locale, final String brand, final String country) {
		try {
			String cacheKey = brand + "_" + locale + "_" + choicifyAppService.getJsonNodeName(selectors);
			TargetColors targetColors = choicifyAppService
					.searchTargetHairColorTagsOnPages(resourceResolver, selectors, locale, brand, country);
			storeTargetResultsCache(cacheKey, targetColors);
		} catch (RepositoryException ex) {
			log.error("RepositoryException {}", ex);
		}
	}

	private static void storeTargetResultsCache(String cacheKey, TargetColors targetColors) {
		if (targetColors != null) {
			EhCacheService.targetColorsCache.put(cacheKey, targetColors);
		}
	}

	private static void cacheNaturalHairColorTags(final ResourceResolver resourceResolver,
			final ChoicifyColorsAppService choicifyAppService, final String locale, final String brand,
			final String country) throws RepositoryException {
		log.debug("Start: NaturalHairColorTags");
		String cacheKey =
				brand + "_" + locale + "_" + ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR;
		NaturalColors naturalColors = choicifyAppService
				.searchNaturalHairColorTagsOnPages(resourceResolver, locale, brand, country);

		// caching results
		if (naturalColors != null) {
			EhCacheService.naturalColorsCache.put(cacheKey, naturalColors);
		}
		log.debug("End: NaturalHairColorTags");
	}

	private static void cacheTargetNaturalHairColorTags(final ResourceResolver resourceResolver,
			final ChoicifyColorsAppService choicifyAppService, final String locale, final String brand,
			final String country) throws RepositoryException {
		log.debug("Start: TargetNaturalHairColors");
		String cacheKey = brand + "_" + locale + "_" + ChoicifyAppConstants.TARGET_NATURAL_COLORS;
		TargetNaturalColors targetNaturalColors = choicifyAppService
				.searchTargetNaturalColorTagsOnPages(resourceResolver, locale, brand, country);

		// caching results
		if (targetNaturalColors != null) {
			EhCacheService.targetNaturalColorsCache.put(cacheKey, targetNaturalColors);
		}
		log.debug("End: TargetNaturalHairColors");
	}
}
