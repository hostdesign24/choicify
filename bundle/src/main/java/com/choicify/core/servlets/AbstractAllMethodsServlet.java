package com.choicify.core.servlets;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import com.choicify.core.utils.ResourceResolverUtils;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Component
public class AbstractAllMethodsServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	protected static String DIFF_POOL = "DIFF_POOL";

	protected static final String EXPIRES_NAME = "Expires";
	protected static final String EXPIRES_FORMAT = "EEE, dd MMM yyyy HH:mm:ss z";

	protected static final String CACHE_CONTROL_NAME = "Cache-Control";
	protected static final String HEADER_PREFIX = "max-age=";

	private Calendar expiresTime = Calendar.getInstance();

	public <T> T getService(final SlingHttpServletRequest request, final Class<T> clazz) {
		return request.getResource().adaptTo(clazz);
	}

	protected ResourceResolver getResourceResolver(final String service) throws LoginException {
		return ResourceResolverUtils.getServiceResourceResolver(this.resourceResolverFactory, service);
	}

	protected String getCacheControlHeaderName() {
		return CACHE_CONTROL_NAME;
	}

	protected String getCacheControlHeaderValue() {
		return HEADER_PREFIX + 1000; // in seconds
	}

	protected String getExpireHeaderName() {
		return EXPIRES_NAME;
	}

	protected String getExpireHeaderValue() {
		Calendar next = Calendar.getInstance();
		next.set(Calendar.HOUR_OF_DAY, expiresTime.get(Calendar.HOUR_OF_DAY));
		next.set(Calendar.MINUTE, expiresTime.get(Calendar.MINUTE));
		next.set(Calendar.SECOND, 0);

		adjustExpires(next);

		SimpleDateFormat dateFormat = new SimpleDateFormat(EXPIRES_FORMAT);
		dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormat.format(next.getTime());
	}

	private void adjustExpires(Calendar next) {
		next.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		if (next.before(Calendar.getInstance())) {
			next.add(Calendar.DAY_OF_WEEK, next.getMaximum(Calendar.DAY_OF_WEEK));;
		}
	}
}
