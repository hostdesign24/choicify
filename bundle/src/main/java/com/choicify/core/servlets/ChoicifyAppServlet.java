package com.choicify.core.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.enums.ChoicifyCategoryType;
import com.choicify.core.enums.ChoicifyCountryType;
import com.choicify.core.jmx.ClearEhCachesMBean;
import com.choicify.core.schedular.ChoicifyCacheScheduler;
import com.choicify.core.services.ChoicifyColorsAppService;
import com.choicify.core.utils.LangUtils;
import com.choicify.core.utils.ResourceResolverUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * Choicify Product Search Servlet. This Servlet searched product pages tagged with choicify colors
 * and shades. Request comes from Choicify Application and Response is json object with product
 * parameters and filter.
 * 
 * @author HostDesign24.com
 *
 */
@Slf4j
@SlingServlet(resourceTypes = "sling/servlet/default", selectors = {"choicify"},
		extensions = "json", methods = {"GET", "POST"})
public class ChoicifyAppServlet extends AbstractAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	@Reference
	private ChoicifyColorsAppService choicifyColorsAppService;

	@Reference
	private ClearEhCachesMBean clearEhCacheService;

	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	private BundleContext bundleContext;

	private static final List<String> validselectors = Arrays.asList("choicify", "naturalcolors",
			"products", "targetcolors", "generateCachedResults", "cleanCachedResults",
			ChoicifyAppConstants.TARGET_NATURAL_COLORS);

	@Activate
	public void activate(final ComponentContext componentContext) {
		this.bundleContext = componentContext.getBundleContext();
	}

	@Deactivate
	public void deactivate(final ComponentContext componentContext) {
		this.bundleContext = null;
	}

	@Override
	protected void doGet(final SlingHttpServletRequest request,
			final SlingHttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		handleCORS(response);
		String results = StringUtils.EMPTY;
		final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());

		String locale;
		String brand;
		String category;
		String country;

		if (LangUtils.isCategorySelectorExist(request.getRequestURI(),
				choicifyColorsAppService.getCategories())) {
			locale = LangUtils.getLanguageCodeFromSelector(selectors);
		} else {
			locale = LangUtils.getLanguageCodeForOldPattern(selectors);
		}
		ResourceResolver resourceResolver = null;
		try {
			resourceResolver = request.getResourceResolver();
			if (!isAcceptedSelectors(selectors, locale, resourceResolver)) {
				log.debug("Selectors not valid {}", selectors);
				// Method not allowed
				response.setStatus(405);
				response.getWriter().print("{}");
				return;
			}

			if (LangUtils.isCategorySelectorExist(request.getRequestURI(),
					choicifyColorsAppService.getCategories())) {
				brand = LangUtils.getMarketFromSelector(selectors);
				locale = LangUtils.getLanguageCodeFromSelector(selectors, brand);
				category = LangUtils.getCategoryFromSelector(selectors);
				country = LangUtils.getCountryFromSelector(selectors);
			} else {
				brand = LangUtils.getBrandForOldPattern(selectors);
				locale = LangUtils.getLanguageCodeForOldPattern(selectors, brand);
				country = ChoicifyCountryType.DE.getCountryType();
				category = ChoicifyCategoryType.HAIRCOLOR.getCategoryType();
			}

			// generate eh cache results
			if (selectors.contains(ChoicifyAppConstants.SELECTOR_GENERATE_CACHED_RESULTS)) {

				final ChoicifyCacheScheduler choicifyCacheScheduler = this.getWarmCacheScheduler();
				if (ChoicifyCacheScheduler.isRunning()) {
					response.getWriter().println(
							"{\"status\":\"ChoicifyCacheScheduler is currently running. please try again later.\"}");

				} else {
					Thread thread = new Thread(choicifyCacheScheduler);
					thread.start();
					response.getWriter().println("{\"status\":\"Started\"}");
				}
				return;
			} else if (selectors.contains(ChoicifyAppConstants.SELECTOR_CLEAN_CACHES)) {
				clearEhCacheService.clearAllCaches();
				response.getWriter().println("{\"status\":\"Caches are cleared\"}");
				return;
			}

			// haircolors
			if (category.equals(ChoicifyCategoryType.HAIRCOLOR.getCategoryType())) {
				if (selectors.contains(ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR)
						&& selectors.size() >= 2) {
					results = choicifyColorsAppService.getNaturalHairColorTagsOnPages(locale, brand, country,
							resourceResolver);

				} else if (selectors.contains(ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR)
						&& selectors.size() >= 2) {
					results = choicifyColorsAppService.getTargetHairColorTagsOnPages(selectors, locale, brand,
							country, resourceResolver);

				} else if (selectors.contains(ChoicifyAppConstants.TARGET_NATURAL_COLORS)
						&& selectors.size() >= 2) {
					results = choicifyColorsAppService.getTargetNaturalColorTagsOnPages(selectors, locale,
							brand, country, resourceResolver);

				} else if (selectors.contains(ChoicifyAppConstants.PRODUCTS_SELECTOR)
						&& selectors.size() > 2) {
					results = choicifyColorsAppService.getProudctPagesByTags(selectors, locale, brand,
							country, resourceResolver);
				}
			}

		} finally {
			ResourceResolverUtils.close(resourceResolver);
		}

		response.getWriter().print(results);
	}

	/**
	 * Handle Cross Origin Resource.
	 * 
	 * @param request
	 * @param response
	 */
	private void handleCORS(SlingHttpServletResponse response) {
		// cache headers
		response.setHeader(getExpireHeaderName(), getExpireHeaderValue());
		response.setHeader(getCacheControlHeaderName(), getCacheControlHeaderValue());
	}

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	@Override
	protected void doOptions(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		handleCORS(response);
	}

	/**
	 * Check if selector is a valid one to search for product.
	 * 
	 * @param selector {@link String}
	 * @return {@link Boolean}
	 */
	private boolean isAcceptedSelectors(List<String> selectors, String language,
			ResourceResolver resourceResolver) {
		for (String selector : selectors) {
			boolean validSelector = false;
			if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
					|| StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)
					|| StringUtils.startsWith(selector,
							ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
				validSelector = true;
			}
			if (validselectors.contains(selector)
					|| choicifyColorsAppService.getAllowedsites().contains(selector)
					|| choicifyColorsAppService.getCategories().contains(selector)
					|| choicifyColorsAppService.getAvailableCountrys(resourceResolver).contains(selector)
					|| StringUtils.equals(selector, language)) {
				validSelector = true;
			}

			if (!validSelector) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	protected ChoicifyCacheScheduler getWarmCacheScheduler() {
		ChoicifyCacheScheduler result = null;
		ServiceReference<?>[] serviceReferences;
		try {
			serviceReferences = bundleContext.getServiceReferences(Runnable.class.getName(), null);
			Runnable tempConfiguration = null;
			for (ServiceReference reference : serviceReferences) {
				tempConfiguration = (Runnable) bundleContext.getService(reference);
				if (tempConfiguration.getClass() == ChoicifyCacheScheduler.class) {
					result = (ChoicifyCacheScheduler) tempConfiguration;
				}
			}
		} catch (InvalidSyntaxException ex) {
			log.error("InvalidSyntaxException {}", ex);
		}
		return result;
	}

	protected ResourceResolver getResourceResolver(final String service) throws LoginException {
		return ResourceResolverUtils.getServiceResourceResolver(this.resourceResolverFactory, service);
	}
}
