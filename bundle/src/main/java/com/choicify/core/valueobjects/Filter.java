package com.choicify.core.valueobjects;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Data
public class Filter {

	@Expose
	private String name;
	@Expose
	private String key;
	@Expose
	private String type = "select";
	@Expose
	private List<FilterOption> options = new ArrayList<>();
}
