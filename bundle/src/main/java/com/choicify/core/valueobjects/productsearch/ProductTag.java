package com.choicify.core.valueobjects.productsearch;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.StringUtils;
import com.choicify.core.valueobjects.FilterOption;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Data
public class ProductTag {
	@Expose
	private String key;
	@Expose
	private String name;
	@Expose
	private List<FilterOption> options = new ArrayList<>();

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ProductTag) {
			return StringUtils.equals(((ProductTag) obj).getKey(), this.key);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.key.hashCode();
	}

}
