package com.choicify.core.valueobjects.productsearch;

import java.util.ArrayList;
import java.util.List;
import com.choicify.core.valueobjects.Filter;
import com.choicify.core.valueobjects.fallbackmessage.MissingColor;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Data
public class ProductResult {
	@Expose
	private List<ProductSearchModel> products = new ArrayList<>();

	@Expose
	private List<Filter> filters = new ArrayList<>();

	@Expose
	private List<MissingColor> missingColors;

}
