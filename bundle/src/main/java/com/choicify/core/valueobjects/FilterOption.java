package com.choicify.core.valueobjects;

import org.apache.commons.codec.binary.StringUtils;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Data
public class FilterOption {

	@Expose
	private String key;
	@Expose
	private String name;

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FilterOption) {
			return StringUtils.equals(((FilterOption) obj).getKey(), this.key);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.key.hashCode();
	}

}
