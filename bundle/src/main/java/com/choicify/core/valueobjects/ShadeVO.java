package com.choicify.core.valueobjects;

import lombok.Data;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Data
public class ShadeVO {
  
  private String nameShade;
  private String labelShade;
  private String urlShade;
  private String urlShadeHead;
	private Integer productsCount;
  private String perfectCamColorTile;
  private String perfectCamColorCode;
}
