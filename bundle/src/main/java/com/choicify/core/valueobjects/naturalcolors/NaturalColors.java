package com.choicify.core.valueobjects.naturalcolors;

import java.util.ArrayList;
import java.util.List;
import com.choicify.core.valueobjects.ColorVO;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Data
public class NaturalColors {
	@Expose
	private List<ColorVO> nhcWithNhs = new ArrayList<>();
}
