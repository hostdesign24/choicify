package com.choicify.core.valueobjects;

import java.util.List;
import lombok.Data;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Data
public class ColorVO {

	private String urlColor;
	private String urlColorHead;
	private String nameColor;
	private String labelColor;
	private Integer productsCount;
	private List<ShadeVO> shades;

}
