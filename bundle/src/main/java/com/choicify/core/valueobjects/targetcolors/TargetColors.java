package com.choicify.core.valueobjects.targetcolors;

import java.util.ArrayList;
import java.util.List;
import com.choicify.core.valueobjects.ColorVO;
import com.google.gson.annotations.Expose;
import lombok.Data;

/**
 * 
 * @author HostDesign24.com
 *
 */
@Data
public class TargetColors {
	@Expose
	private List<ColorVO> nhcWithNhs = new ArrayList<>();

	@Expose
	private List<ColorVO> thcWithThs = new ArrayList<>();
}
