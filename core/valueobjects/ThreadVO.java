package com.choicify.core.valueobjects;

import java.io.InputStream;
import javax.jcr.Session;
import org.apache.sling.api.resource.ResourceResolver;
import com.choicify.core.services.ChoicifyAppService;
import com.choicify.core.services.ChoicifyReplicationService;

public class ThreadVO {

  private InputStream stream;
  private ResourceResolver resourceResolver;
  private Session session;
  private Boolean deleteAction;
  private ChoicifyAppService choicifyAppService;
  private ChoicifyReplicationService choicifyReplicationService;

  public InputStream getStream() {
    return stream;
  }

  public void setStream(InputStream stream) {
    this.stream = stream;
  }

  public ResourceResolver getResourceResolver() {
    return resourceResolver;
  }

  public void setResourceResolver(ResourceResolver resourceResolver) {
    this.resourceResolver = resourceResolver;
  }

  public Session getSession() {
    return session;
  }

  public void setSession(Session session) {
    this.session = session;
  }

  public Boolean getDeleteAction() {
    return deleteAction;
  }

  public void setDeleteAction(Boolean deleteAction) {
    this.deleteAction = deleteAction;
  }

  public ChoicifyAppService getChoicifyAppService() {
    return choicifyAppService;
  }

  public void setChoicifyAppService(ChoicifyAppService choicifyAppService) {
    this.choicifyAppService = choicifyAppService;
  }

  public ChoicifyReplicationService getChoicifyReplicationService() {
    return choicifyReplicationService;
  }

  public void setChoicifyReplicationService(ChoicifyReplicationService choicifyReplicationService) {
    this.choicifyReplicationService = choicifyReplicationService;
  }

}
