package com.choicify.core.valueobjects;

import java.util.List;

public class ColorVO {

  private String urlColor;
  private String urlColorHead;
  private String nameColor;
  private int productsCount;
  List<ShadeVO> shades;

  public String getUrlColor() {
    return urlColor;
  }

  public void setUrlColor(String urlColor) {
    this.urlColor = urlColor;
  }

  public String getUrlColorHead() {
    return urlColorHead;
  }

  public void setUrlColorHead(String urlColorHead) {
    this.urlColorHead = urlColorHead;
  }

  public String getNameColor() {
    return nameColor;
  }

  public void setNameColor(String nameColor) {
    this.nameColor = nameColor;
  }

  public void setProductsCount(int productsCount) {
    this.productsCount = productsCount;
  }

  public int getProductsCount() {
    return productsCount;
  }

  public List<ShadeVO> getShades() {
    return shades;
  }

  public void setShades(List<ShadeVO> shades) {
    this.shades = shades;
  }

}
