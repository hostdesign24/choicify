package com.choicify.core.valueobjects;

public class CsvImportVO {


  private String naturalHairColor;
  private String naturalHairShade;
  private String targetHairColor;
  private String targetHairShade;
  private String durability;
  private String gtin;
  private String pagePath;
  private boolean isPageActivated;


  public String getNaturalHairColor() {
    return naturalHairColor;
  }

  public void setNaturalcHairColor(String naturalHairColor) {
    this.naturalHairColor = naturalHairColor;
  }

  public String getNaturalHairShade() {
    return naturalHairShade;
  }

  public void setNaturalHairShade(String naturalHairShade) {
    this.naturalHairShade = naturalHairShade;
  }

  public String getTargetHairColor() {
    return targetHairColor;
  }

  public void setTargetHairColor(String targetHairColor) {
    this.targetHairColor = targetHairColor;
  }

  public String getTargetHairShade() {
    return targetHairShade;
  }

  public void setTargetHairShade(String targetHairShade) {
    this.targetHairShade = targetHairShade;
  }

  public String getDurability() {
    return durability;
  }

  public void setDurability(String durability) {
    this.durability = durability;
  }

  public String getGtin() {
    return gtin;
  }

  public void setGtin(String gtin) {
    this.gtin = gtin;
  }

  public String getPagePath() {
    return pagePath;
  }

  public void setPagePath(String pagePath) {
    this.pagePath = pagePath;
  }

  public boolean isPageActivated() {
    return isPageActivated;
  }

  public void setPageActivated(boolean isPageActivated) {
    this.isPageActivated = isPageActivated;
  }

}
