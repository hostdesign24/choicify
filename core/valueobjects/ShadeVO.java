package com.choicify.core.valueobjects;

public class ShadeVO {
  private String nameShade;
  private String urlShade;
  private String urlShadeHead;
  private int productsCount;

  public String getNameShade() {
    return nameShade;
  }

  public void setNameShade(String nameShade) {
    this.nameShade = nameShade;
  }

  public String getUrlShade() {
    return urlShade;
  }

  public void setUrlShade(String urlShade) {
    this.urlShade = urlShade;
  }

  public String getUrlShadeHead() {
    return urlShadeHead;
  }

  public void setUrlShadeHead(String urlShadeHead) {
    this.urlShadeHead = urlShadeHead;
  }

  public void setProductsCount(int productsCount) {
    this.productsCount = productsCount;
  }

  public int getProductsCount() {
    return productsCount;
  }
}
