package com.choicify.core.valueobjects;

public class CsvProductImportVO {

  private String country;
  private String divison;
  private String language;
  private String brandTitle;
  private String category;
  private String productLineTitle;
  private String gtin;
  private String productTitle;
  private String status;
  private String imagePath;
  private String productUrl;

  /**
   * @return the country
   */
  public final String getCountry() {
    return country;
  }

  /**
   * @param country the country to set
   */
  public final void setCountry(String country) {
    this.country = country;
  }

  /**
   * @return the divison
   */
  public final String getDivison() {
    return divison;
  }

  /**
   * @param divison the divison to set
   */
  public final void setDivison(String divison) {
    this.divison = divison;
  }

  /**
   * @return the language
   */
  public final String getLanguage() {
    return language;
  }

  /**
   * @param language the language to set
   */
  public final void setLanguage(String language) {
    this.language = language;
  }

  /**
   * @return the brandTitle
   */
  public final String getBrandTitle() {
    return brandTitle;
  }

  /**
   * @param brandTitle the brandTitle to set
   */
  public final void setBrandTitle(String brandTitle) {
    this.brandTitle = brandTitle;
  }

  /**
   * @return the category
   */
  public final String getCategory() {
    return category;
  }

  /**
   * @param category the category to set
   */
  public final void setCategory(String category) {
    this.category = category;
  }

  /**
   * @return the productLineTitle
   */
  public final String getProductLineTitle() {
    return productLineTitle;
  }

  /**
   * @param productLineTitle the productLineTitle to set
   */
  public final void setProductLineTitle(String productLineTitle) {
    this.productLineTitle = productLineTitle;
  }

  /**
   * @return the gtin
   */
  public final String getGtin() {
    return gtin;
  }

  /**
   * @param gtin the gtin to set
   */
  public final void setGtin(String gtin) {
    this.gtin = gtin;
  }

  /**
   * @return the productTitle
   */
  public final String getProductTitle() {
    return productTitle;
  }

  /**
   * @param productTitle the productTitle to set
   */
  public final void setProductTitle(String productTitle) {
    this.productTitle = productTitle;
  }

  /**
   * @return the status
   */
  public final String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public final void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the imagePath
   */
  public final String getImagePath() {
    return imagePath;
  }

  /**
   * @param imagePath the imagePath to set
   */
  public final void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  /**
   * @return the productUrl
   */
  public final String getProductUrl() {
    return productUrl;
  }

  /**
   * @param productUrl the productUrl to set
   */
  public final void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

}
