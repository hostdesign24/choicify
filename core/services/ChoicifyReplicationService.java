package com.choicify.core.services;

import javax.jcr.Session;
import com.day.cq.replication.ReplicationException;

public interface ChoicifyReplicationService {

  public void replicate(Session session, String path) throws ReplicationException;

  public void deactivate(Session session, String pagePath) throws ReplicationException;
}
