package com.choicify.core.services.runnable;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.ReferentialIntegrityException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.utils.ResourceResolverUtils;
import com.choicify.core.valueobjects.CsvImportVO;
import com.choicify.core.valueobjects.ThreadVO;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.search.result.Hit;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * Service for processing CSV file containing Chades and Colors.
 *
 * @author christophernde
 *
 */

public class ChoicifyMetadataImporterService implements Runnable {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(ChoicifyMetadataImporterService.class);
  private ThreadVO threadVO;

  public Map<String, List<CsvImportVO>> processSpreadSheet(InputStream inputStream,
      ResourceResolver resourceResolver, Session session, boolean deleteAction)
      throws AccessDeniedException, VersionException, LockException, ConstraintViolationException,
      RepositoryException, BiffException, IOException, ReplicationException {
    Map<String, List<CsvImportVO>> mapProducts = new HashMap<String, List<CsvImportVO>>();
    Map<String, List<CsvImportVO>> mapResults = new HashMap<String, List<CsvImportVO>>();
    List<CsvImportVO> publishedPages = new ArrayList<CsvImportVO>();
    List<CsvImportVO> rejectedPages = new ArrayList<CsvImportVO>();
    List<CsvImportVO> noProductFound = new ArrayList<CsvImportVO>();
    LOGGER.debug("GET THE SPREADSHEET STREAM33");
    Workbook workbook = Workbook.getWorkbook(inputStream);
    LOGGER.debug("GET THE STREAMWorkbook");
    Sheet sheet = workbook.getSheet(0);
    int totalNoOfRows = sheet.getRows();
    LOGGER.debug("GET THE STREAMWorkbook");
    LOGGER.debug("GET THE STREAM44");
    PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
    List<String> replicationPagePaths = new ArrayList<String>();
    for (int row = 1; row < totalNoOfRows; row++) {
      CsvImportVO csvImportVO = readCsvFileContent(sheet, row);
      if (!isCsvFileContentValid(csvImportVO)) {
        LOGGER.debug("CSV file content has empta line.");
        break;
      }
      if (StringUtils.isEmpty(csvImportVO.getGtin())) {
        LOGGER.debug("GTin is not avaliable");
        continue;
      }
      createJcrTagPropertyValue(csvImportVO, resourceResolver);
      List<Hit> searchResult =
          getThreadVO().getChoicifyAppService().searchProductPageByPropertyAndValue(
              resourceResolver, ChoicifyAppConstants.CHOICIFY_GTIN_NAME, csvImportVO.getGtin());

      List<Resource> resources =
          getThreadVO().getChoicifyAppService().getResourcesFromSearchResult(searchResult);
      if (resources != null && resources.size() == 0) {
        LOGGER.debug("Product not available for given Gtin {}", csvImportVO.getGtin());
        noProductFound.add(csvImportVO);
        continue;
      }
      for (Resource resource : resources) {
        Node product = resource.adaptTo(Node.class);
        csvImportVO.setPagePath(pageManager.getContainingPage(resource).getPath());
        if (!publishPage(pageManager, resource) && !StringUtils.contains(resource.getPath(),
            ChoicifyAppConstants.CHOICIFY_PRODUCT_ROOT_PATH)) {
          LOGGER.debug("Create Shade and colors only for activated pages {} ", resource.getPath());
          csvImportVO.setPageActivated(false);
          rejectedPages.add(csvImportVO);
          continue;
        }
        String productPath = product.getPath() + ChoicifyAppConstants.CHOICIFY_ROOT_PATH;
        if (deleteAction) {
          getThreadVO().getChoicifyReplicationService().deactivate(session, productPath);
          deleteNode(productPath, resourceResolver, session);
        } else {
          Node tagNode = createNodeForShadeAndColorTags(csvImportVO, mapProducts, product.getPath(),
              resourceResolver, session, deleteAction);
          storeFilters(csvImportVO, product.getPath(), session, resourceResolver);
          storeShadesAndColors(csvImportVO, tagNode);
        }
        String replicationPath = resource.getPath() + ChoicifyAppConstants.CHOICIFY_ROOT_PATH;
        replicationPagePaths.add(replicationPath);
        csvImportVO.setPageActivated(true);
        publishedPages.add(csvImportVO);
      }
    }
    session.save();
    mapResults.put("failure", rejectedPages);
    mapResults.put("success", publishedPages);
    mapResults.put("noProduct", noProductFound);
    saveChoicifyResults(session, mapResults);

    if (deleteAction) {
      deactivateCreatedColors(session, replicationPagePaths);
    } else {
      replicateCreatedColors(session, replicationPagePaths);
    }

    return mapResults;
  }

  /**
   * Store choicify Results temporally "/var/choicify/results".
   *
   * @param session
   * @param mapResults
   * @throws RepositoryException
   * @throws ValueFormatException
   * @throws VersionException
   * @throws LockException
   * @throws ConstraintViolationException
   * @throws AccessDeniedException
   * @throws ItemExistsException
   * @throws ReferentialIntegrityException
   * @throws InvalidItemStateException
   * @throws NoSuchNodeTypeException
   */
  private void saveChoicifyResults(Session session, Map<String, List<CsvImportVO>> mapResults)
      throws RepositoryException, ValueFormatException, VersionException, LockException,
      ConstraintViolationException, AccessDeniedException, ItemExistsException,
      ReferentialIntegrityException, InvalidItemStateException, NoSuchNodeTypeException {
    String results = new Gson().toJson(mapResults);
    Node resultNode = JcrUtils.getOrCreateByPath("/var/choicify/results", false, "nt:unstructured",
        "nt:unstructured", session, true);
    resultNode.setProperty("results", results);
    session.save();
  }

  /**
   * Publish pages caontaining newly created colors and shades.
   *
   * @param session
   * @param replicationPagePaths
   * @throws ReplicationException
   */
  private void replicateCreatedColors(Session session, List<String> replicationPagePaths)
      throws ReplicationException {
    Set<String> pagePathsWithoutDuplicates = new LinkedHashSet<String>(replicationPagePaths);
    LOGGER.debug("Start Replication.");
    for (String pagePath : pagePathsWithoutDuplicates) {
      getThreadVO().getChoicifyReplicationService().replicate(session, pagePath);
    }
    LOGGER.debug("Finish replication.");
  }

  /**
   * Deactivate page content caontaining newly created colors and shades.
   * 
   * @param session
   * @param replicationPagePaths
   * @throws ReplicationException
   */
  private void deactivateCreatedColors(Session session, List<String> replicationPagePaths)
      throws ReplicationException {
    Set<String> pagePathsWithoutDuplicates = new LinkedHashSet<String>(replicationPagePaths);
    LOGGER.debug("Start Replication.");
    for (String pagePath : pagePathsWithoutDuplicates) {
      getThreadVO().getChoicifyReplicationService().deactivate(session, pagePath);
    }
    LOGGER.debug("Finish replication.");
  }

  /**
   * Verify iif page should be activated and colors and shade should also be created.
   *
   * @param pageManager {@link PageManager}
   * @param resource {@link Resource}
   * @return {@link Boolean}
   */
  private boolean publishPage(PageManager pageManager, Resource resource) {
    Page page = pageManager.getContainingPage(resource);
    if (page == null) {
      return false;
    }
    ReplicationStatus replicationStatus = page.adaptTo(ReplicationStatus.class);
    if (replicationStatus == null) {
      return false;
    }
    return replicationStatus.isActivated();
  }

  /**
   * Store product filters.
   *
   * @param csvImportVO {@link CsvImportVO}
   * @param productPath {@link String}
   * @param session {@link Session}
   * @param resourceResolver {@link ResourceResolver}
   * @throws RepositoryException {@link RepositoryException}
   */
  private void storeFilters(CsvImportVO csvImportVO, String productPath, Session session,
      ResourceResolver resourceResolver) throws RepositoryException {
    String filterPath = productPath + ChoicifyAppConstants.STRING_SEPERATOR_SLASH
        + ChoicifyAppConstants.CHOICIFY_ROOT_PATH + ChoicifyAppConstants.STRING_SEPERATOR_SLASH
        + ChoicifyAppConstants.JSON_NAME_PRODUCT_FILTER;
    String durability = csvImportVO.getDurability();
    String tagPath = ChoicifyAppConstants.TAG_DURABILITY_ROOT_PATH + durability;
    Resource resource = resourceResolver.getResource(tagPath);
    if (resource == null) {
      LOGGER.debug("Tag Durability not available {}", tagPath);
      return;
    }
    Tag tag = resource.adaptTo(Tag.class);
    String tagValue = getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(tag);
    if (StringUtils.equals(durability, ChoicifyAppConstants.CHOICIFY_TAG_PERMANENT_NAME)
        || StringUtils.equals(durability, ChoicifyAppConstants.CHOICIFY_TAG_TEMPORARY_NAME)) {
      String durabilityTagPath = filterPath + ChoicifyAppConstants.STRING_SEPERATOR_SLASH
          + ChoicifyAppConstants.CHOICIFY_TAG_DURABILITY_NAME;

      Resource resourceDurability = resourceResolver.getResource(durabilityTagPath);
      if (resourceDurability != null) {
        LOGGER.debug("Durability of product already created {}", resourceDurability.getPath());
        return;
      }
      Node durabilityNode = JcrUtils.getOrCreateByPath(durabilityTagPath, true, "nt:unstructured",
          "nt:unstructured", session, true);
      durabilityNode.setProperty(ChoicifyAppConstants.CHOICIFY_TAG_DURABILITY_NAME, tagValue);
    }
  }

  /**
   * Store Shades And Colors under product node.
   *
   * @param csvImportVO {@link CsvImportVO}
   * @param tagNode {@link Node}
   * @throws RepositoryException {@link RepositoryException}
   * @throws ConstraintViolationException {@link ConstraintViolationException}
   * @throws LockException {@link LockException}
   * @throws VersionException {@link VersionException}
   * @throws ValueFormatException {@linkValueFormatException}
   */
  private void storeShadesAndColors(CsvImportVO csvImportVO, Node tagNode)
      throws ValueFormatException, VersionException, LockException, ConstraintViolationException,
      RepositoryException {
    if (tagNode == null) {
      return;
    }
    if (StringUtils.isNotEmpty(csvImportVO.getNaturalHairColor())) {
      tagNode.setProperty(ChoicifyAppConstants.NATURAL_HAIR_COLORS_PROP_NAME,
          csvImportVO.getNaturalHairColor());
    }
    if (StringUtils.isNotEmpty(csvImportVO.getNaturalHairShade())) {
      tagNode.setProperty(ChoicifyAppConstants.NATURAL_HAIR_SHADES_PROP_NAME,
          csvImportVO.getNaturalHairShade());
    }
    if (StringUtils.isNotEmpty(csvImportVO.getTargetHairColor())) {
      tagNode.setProperty(ChoicifyAppConstants.TARGET_HAIR_COLORS_PROP_NAME,
          csvImportVO.getTargetHairColor());
    }
    if (StringUtils.isNotEmpty(csvImportVO.getTargetHairShade())) {
      tagNode.setProperty(ChoicifyAppConstants.TARGET_HAIR_SHADES_PROP_NAME,
          csvImportVO.getTargetHairShade());
    }
  }

  /**
   * Create jcr node for Shades And Colors under product node.
   *
   * @param csvImportVO {@link CsvImportVO}
   * @param mapProducts {@link List}t
   * @param productPath {@link String}
   * @param resourceResolver {@link ResourceResolver}
   * @param session {@link Session}
   * @param deleteAction {@link Boolean}
   * @throws AccessDeniedException {@link AccessDeniedException}
   * @throws VersionException {@link VersionException}
   * @throws LockException {@link LockException}
   * @throws ConstraintViolationException {@link ConstraintViolationException}
   * @throws RepositoryException {@link RepositoryException}
   */
  private Node createNodeForShadeAndColorTags(CsvImportVO csvImportVO,
      Map<String, List<CsvImportVO>> mapProducts, String productPath,
      ResourceResolver resourceResolver, Session session, boolean deleteAction)
      throws AccessDeniedException, VersionException, LockException, ConstraintViolationException,
      RepositoryException {
    UUID uuid = UUID.randomUUID();
    String tagNodeName = "tags_" + StringUtils.substring(uuid.toString(), 2, 10);
    String tagRootPath = ChoicifyAppConstants.PRODUCT_COLORATIONS_PATH;
    String tagPath = productPath + tagRootPath + tagNodeName;
    if (deleteAction) {
      deleteNode((productPath + ChoicifyAppConstants.CHOICIFY_ROOT_PATH), resourceResolver,
          session);
      return null;
    }
    if (StringUtils.isEmpty(csvImportVO.getNaturalHairColor())
        && StringUtils.isEmpty(csvImportVO.getNaturalHairShade())
        && StringUtils.isEmpty(csvImportVO.getTargetHairColor())
        && StringUtils.isEmpty(csvImportVO.getTargetHairShade())) {
      return null;
    }
    deleteExistingNode((productPath + tagRootPath), csvImportVO, mapProducts, resourceResolver,
        session);
    createCsvImporterObject(csvImportVO, mapProducts);
    return JcrUtils.getOrCreateByPath(tagPath, false, "nt:unstructured", "nt:unstructured", session,
        true);
  }

  /**
   * @param tagPath {@link String}
   * @param csvImportVO {@link CsvImportVO}
   * @param mapProducts {@link List}t
   * @param resourceResolver {@link ResourceResolver}
   * @param deleteAction {@link Boolean}
   * @throws AccessDeniedException {@link AccessDeniedException}
   * @throws VersionException {@link VersionException}
   * @throws LockException {@link LockException}
   * @throws ConstraintViolationException {@link ConstraintViolationException}
   * @throws RepositoryException {@link RepositoryException}
   */
  private void deleteExistingNode(String tagPath, CsvImportVO csvImportVO,
      Map<String, List<CsvImportVO>> mapProducts, ResourceResolver resourceResolver,
      Session session) throws AccessDeniedException, VersionException, LockException,
      ConstraintViolationException, RepositoryException {

    if (mapProducts.containsKey(csvImportVO.getGtin())) {
      LOGGER.debug("Product was already deleted: {}" + csvImportVO.getGtin());
      return;
    }
    deleteNode(tagPath, resourceResolver, session);
  }

  /**
   * @param tagPath {@link String}
   * @param session {@link Session}
   * @param resourceResolver {@link ResourceResolver}
   * @throws AccessDeniedException {@link AccessDeniedException}
   * @throws VersionException {@link VersionException}
   * @throws LockException {@link LockException}
   * @throws ConstraintViolationException {@link ConstraintViolationException}
   * @throws RepositoryException {@link RepositoryException}
   */
  private void deleteNode(String tagPath, ResourceResolver resourceResolver, Session session)
      throws VersionException, LockException, ConstraintViolationException, AccessDeniedException,
      RepositoryException, ItemExistsException, ReferentialIntegrityException,
      InvalidItemStateException, NoSuchNodeTypeException {
    if (resourceResolver.getResource(tagPath) != null) {
      session.removeItem(tagPath);
      session.save();
    }
  }

  /**
   * Create tag property values for jcr node.
   *
   * @param csvImportVO {@link CsvImportVO}
   * @param resourceResolver {@link ResourceResolver}
   */
  private void createJcrTagPropertyValue(CsvImportVO csvImportVO,
      ResourceResolver resourceResolver) {
    String nhcPath = ChoicifyAppConstants.ROOT_PATH_NATURAL_HAIR_COLOR
        + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + csvImportVO.getNaturalHairColor();
    Resource naturalHaircolorResource = resourceResolver.getResource(nhcPath);
    Resource naturalHairShadeResource = null;
    if (naturalHaircolorResource != null) {
      Tag nhcTag = naturalHaircolorResource.adaptTo(Tag.class);
      csvImportVO.setNaturalcHairColor(
          getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(nhcTag));
      String nhsPath = naturalHaircolorResource.getPath()
          + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + csvImportVO.getNaturalHairShade();
      naturalHairShadeResource = resourceResolver.getResource(nhsPath);
    } else {
      csvImportVO.setNaturalcHairColor(null);
    }

    if (naturalHairShadeResource != null) {
      Tag nhsTag = naturalHairShadeResource.adaptTo(Tag.class);
      csvImportVO.setNaturalHairShade(
          getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(nhsTag));
    } else {
      csvImportVO.setNaturalHairShade(null);
    }

    String thcPath = ChoicifyAppConstants.ROOT_PATH_TARGET_HAIR_COLOR
        + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + csvImportVO.getTargetHairColor();
    Resource targetHairColorResource = resourceResolver.getResource(thcPath);
    Resource targetHairShadeResource = null;
    if (targetHairColorResource != null) {
      Tag thcTag = targetHairColorResource.adaptTo(Tag.class);
      csvImportVO.setTargetHairColor(
          getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(thcTag));
      String thsPath = targetHairColorResource.getPath()
          + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + csvImportVO.getTargetHairShade();
      targetHairShadeResource = resourceResolver.getResource(thsPath);
    } else {
      csvImportVO.setTargetHairColor(null);
    }

    if (targetHairShadeResource != null) {
      Tag thsTag = targetHairShadeResource.adaptTo(Tag.class);
      csvImportVO.setTargetHairShade(
          getThreadVO().getChoicifyAppService().createTagValueStoredInJcrContent(thsTag));
    } else {
      csvImportVO.setTargetHairShade(null);
    }
  }

  /**
   * Read the content of csv file and store in an onject.
   *
   * @param sheet {@link Sheet}
   * @param index {@link Integer}
   * @return {@link CsvImportVO}
   */
  private CsvImportVO readCsvFileContent(Sheet sheet, int index) {
    CsvImportVO csvImportVO = new CsvImportVO();
    Cell a3 = sheet.getCell(0, index);
    Cell b3 = sheet.getCell(1, index);
    Cell c3 = sheet.getCell(2, index);
    Cell d3 = sheet.getCell(3, index);
    Cell e3 = sheet.getCell(4, index);
    Cell f3 = sheet.getCell(5, index);
    csvImportVO.setNaturalcHairColor(a3.getContents());
    csvImportVO.setNaturalHairShade(b3.getContents());
    csvImportVO.setTargetHairColor(c3.getContents());
    csvImportVO.setTargetHairShade(d3.getContents());
    csvImportVO.setDurability(e3.getContents());
    csvImportVO.setGtin(f3.getContents());
    return csvImportVO;
  }

  /**
   * Read CSV file content and if line is empty, return false.
   *
   * @param csvImportVO {@link CsvImportVO}
   * @return {@link Boolean}
   */
  private boolean isCsvFileContentValid(CsvImportVO csvImportVO) {
    if (StringUtils.isEmpty(csvImportVO.getNaturalHairColor())
        && StringUtils.isEmpty(csvImportVO.getNaturalHairShade())
        && StringUtils.isEmpty(csvImportVO.getTargetHairColor())
        && StringUtils.isEmpty(csvImportVO.getTargetHairShade())) {
      return false;
    }
    return true;
  }

  /**
   * Create CSV Importer Object for json out put.
   *
   * @param csvImportVO {@link CsvImportVO}
   * @param mapProducts {@link List}
   */
  private void createCsvImporterObject(CsvImportVO csvImportVO,
      Map<String, List<CsvImportVO>> mapProducts) {
    List<CsvImportVO> csvImportVOs = new ArrayList<CsvImportVO>();
    if (mapProducts.containsKey(csvImportVO.getGtin())) {
      csvImportVOs = mapProducts.get(csvImportVO.getGtin());
    } else {
      mapProducts.put(csvImportVO.getGtin(), csvImportVOs);
    }
    if (!csvImportVOs.contains(csvImportVO)) {
      csvImportVOs.add(csvImportVO);
    }
  }

  /**
   * Exercute thread importer Service.
   */
  private void exercute() {
    try {
      processSpreadSheet(getThreadVO().getStream(), getThreadVO().getResourceResolver(),
          getThreadVO().getSession(), getThreadVO().getDeleteAction());
    } catch (AccessDeniedException e) {
      LOGGER.error("AccessDeniedException", e);
    } catch (VersionException e) {
      LOGGER.error("VersionException", e);
    } catch (LockException e) {
      LOGGER.error("LockException", e);
    } catch (ConstraintViolationException e) {
      LOGGER.error("ConstraintViolationException", e);
    } catch (BiffException e) {
      LOGGER.error("BiffException", e);
    } catch (RepositoryException e) {
      LOGGER.error("RepositoryException", e);
    } catch (IOException e) {
      LOGGER.error("IOException", e);
    } catch (ReplicationException e) {
      LOGGER.error("ReplicationException", e);
    } finally {
      if (getThreadVO().getSession() != null && getThreadVO().getSession().isLive()) {
        getThreadVO().getSession().logout();
      }
      ResourceResolverUtils.close(getThreadVO().getResourceResolver());
      IOUtils.closeQuietly(getThreadVO().getStream());
    }

  }

  public ThreadVO getThreadVO() {
    return threadVO;
  }

  public void setThreadVO(ThreadVO threadVO) {
    this.threadVO = threadVO;
  }

  public void run() {
    exercute();
  }

}
