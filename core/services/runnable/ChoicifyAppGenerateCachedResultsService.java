package com.choicify.core.services.runnable;

import java.util.ArrayList;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.utils.ResourceResolverUtils;
import com.choicify.core.valueobjects.ThreadVO;

/**
 * Service for processing CSV file containing products.
 *
 * @author christophernde
 *
 */

public class ChoicifyAppGenerateCachedResultsService implements Runnable {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(ChoicifyAppGenerateCachedResultsService.class);
  private ThreadVO threadVO;

  public ThreadVO getThreadVO() {
    return threadVO;
  }

  public void setThreadVO(ThreadVO threadVO) {
    this.threadVO = threadVO;
  }

  /**
   * Run method
   */
  public void run() {
    try {
      execute();
    } catch (JSONException e) {
      LOGGER.error("JSONException", e);
    }
  }

  /**
   * Execute thread importer Service.
   * 
   * @throws JSONException
   */
  private void execute() throws JSONException {
    List<String> selectors = null;
    try {

      // Natural Color
      String resultNhc = getThreadVO().getChoicifyAppService()
          .searchNaturalHairColorTagsOnPages(getThreadVO().getResourceResolver());
      // Path to store results
      String pathNhc = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
          + ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR;
      storeSearchResults(resultNhc, ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, pathNhc,
          getThreadVO().getSession());

      // Target Color & Product search
      Resource resourceRooNhc = getThreadVO().getResourceResolver()
          .getResource(ChoicifyAppConstants.CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR);
      if (resourceRooNhc != null) {
        List<Resource> resourceNhcs = IteratorUtils.toList(resourceRooNhc.listChildren());
        for (Resource resourceNhc : resourceNhcs) {
          selectors = new ArrayList<String>();
          String nhc = resourceNhc.getName();
          selectors.add(nhc);
          // Search Results Target
          String resultsNhc =
              getThreadVO().getChoicifyAppService().searchTargetHairColorTagsOnPages(
                  getThreadVO().getSession(), getThreadVO().getResourceResolver(), selectors);
          // Path to store results Target
          String pathsNhc = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
              + ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR
              + getThreadVO().getChoicifyAppService().createPathFromSelector(selectors);
          storeSearchResults(resultsNhc, ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS,
              pathsNhc, getThreadVO().getSession());

          // Product Nhc
          String resultsProductNhc = getThreadVO().getChoicifyAppService().searchProudctPagesByTags(
              getThreadVO().getSession(), getThreadVO().getResourceResolver(), selectors);
          String productPathNhc =
              ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH + ChoicifyAppConstants.PRODUCTS_SELECTOR
                  + getThreadVO().getChoicifyAppService().createPathFromSelector(selectors);
          storeSearchResults(resultsProductNhc,
              ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, productPathNhc,
              getThreadVO().getSession());

          List<Resource> resourceNhss = IteratorUtils.toList(resourceRooNhc.listChildren());
          for (Resource resourceNhs : resourceNhss) {
            String nhs = resourceNhs.getName();
            selectors.add(nhs);

            // Search Results Target
            String resultsNhs =
                getThreadVO().getChoicifyAppService().searchTargetHairColorTagsOnPages(
                    getThreadVO().getSession(), getThreadVO().getResourceResolver(), selectors);
            // Path to store results Target
            String pathsNhs = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
                + ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR
                + getThreadVO().getChoicifyAppService().createPathFromSelector(selectors);
            storeSearchResults(resultsNhs, ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS,
                pathsNhs, getThreadVO().getSession());

            // Product
            String resultsProductNhs =
                getThreadVO().getChoicifyAppService().searchProudctPagesByTags(
                    getThreadVO().getSession(), getThreadVO().getResourceResolver(), selectors);
            String productPathNhs = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
                + ChoicifyAppConstants.PRODUCTS_SELECTOR
                + getThreadVO().getChoicifyAppService().createPathFromSelector(selectors);
            storeSearchResults(resultsProductNhs,
                ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, productPathNhs,
                getThreadVO().getSession());


            Resource resourceRootThc = getThreadVO().getResourceResolver()
                .getResource(ChoicifyAppConstants.CHOICIFY_TAG_ROOT_PATH_TARGET_HAIR_COLOR);
            List<Resource> resourceThcs = IteratorUtils.toList(resourceRootThc.listChildren());
            for (Resource resourceThc : resourceThcs) {
              String thc = resourceThc.getName();
              selectors.add(thc);

              // Search Results Target
              String resultsThc =
                  getThreadVO().getChoicifyAppService().searchTargetHairColorTagsOnPages(
                      getThreadVO().getSession(), getThreadVO().getResourceResolver(), selectors);
              // Path to store results Target
              String pathsThc = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
                  + ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR
                  + getThreadVO().getChoicifyAppService().createPathFromSelector(selectors);
              storeSearchResults(resultsThc, ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS,
                  pathsThc, getThreadVO().getSession());

              // Product thc
              String resultsProductThc =
                  getThreadVO().getChoicifyAppService().searchProudctPagesByTags(
                      getThreadVO().getSession(), getThreadVO().getResourceResolver(), selectors);
              String productPathThc = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
                  + ChoicifyAppConstants.PRODUCTS_SELECTOR
                  + getThreadVO().getChoicifyAppService().createPathFromSelector(selectors);
              storeSearchResults(resultsProductThc,
                  ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, productPathThc,
                  getThreadVO().getSession());

              List<Resource> resourceThss = IteratorUtils.toList(resourceThc.listChildren());
              for (Resource resourceThs : resourceThss) {
                String ths = resourceThs.getName();
                selectors.add(ths);
                // Search Results Target
                String resultsThs =
                    getThreadVO().getChoicifyAppService().searchTargetHairColorTagsOnPages(
                        getThreadVO().getSession(), getThreadVO().getResourceResolver(), selectors);

                // Path to store results Target
                String pathsThs = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
                    + ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR
                    + getThreadVO().getChoicifyAppService().createPathFromSelector(selectors);
                storeSearchResults(resultsThs,
                    ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, pathsThs,
                    getThreadVO().getSession());

                // Product ths
                String resultsProductThs =
                    getThreadVO().getChoicifyAppService().searchProudctPagesByTags(
                        getThreadVO().getSession(), getThreadVO().getResourceResolver(), selectors);
                String productPathThs = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
                    + ChoicifyAppConstants.PRODUCTS_SELECTOR
                    + getThreadVO().getChoicifyAppService().createPathFromSelector(selectors);
                storeSearchResults(resultsProductThs,
                    ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, productPathThs,
                    getThreadVO().getSession());

              }
            }
          }
        }
      }

      // combindColorsShadesForProductSearch();
      deleteCachedResults(getThreadVO().getResourceResolver(), getThreadVO().getSession());
    } catch (RepositoryException e) {
      LOGGER.error("RepositoryException", e);
    } finally {
      if (getThreadVO().getSession() != null && getThreadVO().getSession().isLive()) {
        getThreadVO().getSession().logout();
      }
      ResourceResolverUtils.close(getThreadVO().getResourceResolver());
      IOUtils.closeQuietly(getThreadVO().getStream());
    }

  }



  /**
   * Delete cached results.
   * 
   * @param resourceResolver {@link ResourceResolver}
   * @param session {@link Session} }
   * @throws RepositoryException {@link RepositoryException
   */
  private void deleteCachedResults(ResourceResolver resourceResolver, Session session)
      throws RepositoryException {
    LOGGER.debug("Cleaning Cached Results.");
    Resource resource =
        resourceResolver.getResource(ChoicifyAppConstants.CHOICIFY_CACHED_RESULT_ROOT_PATH);
    if (resource != null) {
      resource.adaptTo(Node.class).remove();
      session.save();
    }
  }

  /**
   * Store search results temporally to improve performace.
   * 
   * @param result
   * @param name
   * @param path
   * @param resourceResolver
   * @throws RepositoryException
   */
  private void storeSearchResults(String result, String name, String path, Session session)
      throws RepositoryException {
    Node resultNode = JcrUtils.getOrCreateByPath(path, false, "nt:unstructured", "nt:unstructured",
        session, true);
    resultNode.setProperty(name, result);
    session.save();
  }
}
