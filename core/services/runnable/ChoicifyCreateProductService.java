package com.choicify.core.services.runnable;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.utils.NodeUtils;
import com.choicify.core.valueobjects.CsvProductImportVO;
import com.choicify.core.valueobjects.ThreadVO;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;
import com.google.gson.Gson;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 * Service for processing CSV file containing products.
 *
 * @author christophernde
 *
 */

public class ChoicifyCreateProductService implements Runnable {

  private static final Logger LOGGER = LoggerFactory.getLogger(ChoicifyCreateProductService.class);
  private static final String QUERY_STRING =
      "SELECT * FROM [nt:unstructured] as ref WHERE ISDESCENDANTNODE(ref,[%s]) AND "
          + "ref.[gtin] IS NOT NULL";
  private ThreadVO threadVO;

  public ThreadVO getThreadVO() {
    return threadVO;
  }

  public void setThreadVO(ThreadVO threadVO) {
    this.threadVO = threadVO;
  }

  /**
   * Run method
   */
  public void run() {
    execute();
  }

  /**
   * Execute thread importer Service.
   */
  private void execute() {
    try {
      processSpreadSheet(getThreadVO().getStream(), getThreadVO().getResourceResolver(),
          getThreadVO().getSession(), getThreadVO().getDeleteAction());
    } catch (BiffException e) {
      LOGGER.error("BiffException", e);
    } catch (IOException e) {
      LOGGER.error("IOException", e);
    } finally {
      if (getThreadVO().getSession() != null && getThreadVO().getSession().isLive()) {
        getThreadVO().getSession().logout();
      }
      ResourceResolverUtils.close(getThreadVO().getResourceResolver());
      IOUtils.closeQuietly(getThreadVO().getStream());
    }

  }

  /**
   * Method process the spread sheet
   *
   * Create/deletes the product.
   *
   */
  private void processSpreadSheet(InputStream inputStream, ResourceResolver resourceResolver,
      Session session, boolean deleteAction) throws BiffException, IOException {

    List<CsvProductImportVO> successProducts = new ArrayList<CsvProductImportVO>();
    List<CsvProductImportVO> rejectedProducts = new ArrayList<CsvProductImportVO>();
    Map<String, Resource> products = new HashMap<String, Resource>();

    // set all existing products for a division
    setProducts(products, resourceResolver);

    LOGGER.debug("GET THE SPREADSHEET STREAM33");
    Workbook workbook = Workbook.getWorkbook(inputStream);
    LOGGER.debug("GET THE STREAMWorkbook");
    Sheet sheet = workbook.getSheet(0);
    int totalNoOfRows = sheet.getRows();

    for (int row = 1; row < totalNoOfRows; row++) {
      CsvProductImportVO productImportVO = readCsvFileContent(sheet, row);
      if (!isCsvFileContentValid(productImportVO)) {
        LOGGER.debug("CSV file content has empty line.");
        break;
      }
      if (StringUtils.isEmpty(productImportVO.getGtin())) {
        LOGGER.debug("GTin is not avaliable");
        productImportVO.setStatus("Error: GTIN is not avaliable");
        rejectedProducts.add(productImportVO);
        continue;
      }

      if (products.containsKey(productImportVO.getGtin())) {
        LOGGER.debug("Product already exists for the given Gtin {}", productImportVO.getGtin());
        if (deleteAction) {
          deleteProductPage(products.get(productImportVO.getGtin()), productImportVO,
              rejectedProducts, successProducts);
        } else {
          productImportVO
              .setStatus("Error: Product already exists for the Gtin:" + productImportVO.getGtin());
          rejectedProducts.add(productImportVO);
        }
      } else if (!deleteAction) {
        LOGGER.debug("Product not exists for gtin {}; creating new product.",
            productImportVO.getGtin());
        createProductPage(session, productImportVO, rejectedProducts, successProducts);
      }
    }

    try {
      // save product import.
      session.save();

      Map<String, List<CsvProductImportVO>> mapResults =
          new HashMap<String, List<CsvProductImportVO>>();
      mapResults.put("failure", rejectedProducts);
      mapResults.put("success", successProducts);
      saveProductImportResults(session, mapResults);
    } catch (RepositoryException e) {
      LOGGER.error("RepositoryException", e);
    }
  }

  /**
   * Store choicify product import Results temporally "/var/choicify/productimport".
   *
   * @param session
   * @param mapResults
   * @throws RepositoryException
   */
  private void saveProductImportResults(Session session,
      Map<String, List<CsvProductImportVO>> mapResults) throws RepositoryException {
    String results = new Gson().toJson(mapResults);
    Node resultNode = JcrUtils.getOrCreateByPath(ChoicifyAppConstants.CHOICIFY_PRODUCT_RESULT_PATH,
        false, ChoicifyAppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED,
        ChoicifyAppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED, session, true);
    resultNode.setProperty(ChoicifyAppConstants.PROP_NAME_PRODUCT_RESULTS, results);
    session.save();
  }

  /**
   * Read the content of csv file and store in an onject.
   *
   * @param sheet {@link Sheet}
   * @param index {@link Integer}
   * @return {@link CsvProductImportVO}
   */
  private CsvProductImportVO readCsvFileContent(Sheet sheet, int index) {
    CsvProductImportVO productImportVO = new CsvProductImportVO();
    Cell gtin = sheet.getCell(0, index);
    Cell divison = sheet.getCell(1, index);
    Cell country = sheet.getCell(2, index);
    Cell language = sheet.getCell(3, index);
    Cell category = sheet.getCell(4, index);
    Cell brandTitle = sheet.getCell(5, index);
    Cell productLineTitle = sheet.getCell(6, index);
    Cell productTitle = sheet.getCell(7, index);
    Cell imagePath = sheet.getCell(8, index);
    Cell productUrl = sheet.getCell(9, index);

    // setting the values
    productImportVO.setGtin(gtin.getContents());
    productImportVO.setDivison(divison.getContents());
    productImportVO.setCountry(country.getContents());
    productImportVO.setLanguage(language.getContents());
    productImportVO.setCategory(category.getContents());
    productImportVO.setBrandTitle(brandTitle.getContents());
    productImportVO.setProductLineTitle(productLineTitle.getContents());
    productImportVO.setProductTitle(productTitle.getContents());
    productImportVO.setImagePath(imagePath.getContents());
    productImportVO.setProductUrl(productUrl.getContents());

    return productImportVO;
  }

  /**
   * Read CSV file content and if line is empty, return false.
   *
   * @param CsvProductImportVO {@link CsvProductImportVO}
   * @return {@link Boolean}
   */
  private boolean isCsvFileContentValid(CsvProductImportVO productImportVO) {
    if (StringUtils.isEmpty(productImportVO.getGtin())
        && StringUtils.isEmpty(productImportVO.getCountry())
        && StringUtils.isEmpty(productImportVO.getLanguage())
        && StringUtils.isEmpty(productImportVO.getCategory())
        && StringUtils.isEmpty(productImportVO.getBrandTitle())
        && StringUtils.isEmpty(productImportVO.getProductLineTitle())
        && StringUtils.isEmpty(productImportVO.getProductTitle())) {
      return false;
    }
    return true;
  }

  /**
   * Method sets the product resource
   *
   * return Map<String, String>
   */
  private void setProducts(final Map<String, Resource> products,
      final ResourceResolver resourceResolver) {

    String queryString =
        String.format(QUERY_STRING, ChoicifyAppConstants.CHOICIFY_PRODUCT_ROOT_PATH);
    Iterator<Resource> iterator = resourceResolver.findResources(queryString, Query.JCR_SQL2);
    while (iterator.hasNext()) {
      final Resource resource = iterator.next();
      final Node childNode = resource.adaptTo(Node.class);

      if (null == childNode) {
        continue;
      }

      String gtin =
          NodeUtils.getStringPropertyFromNode(childNode, ChoicifyAppConstants.CHOICIFY_GTIN_NAME);

      if (StringUtils.isNotBlank(gtin)) {
        products.put(gtin, resource);
      }

    }

  }

  /**
   * method deletes the product page
   *
   * @param resource
   * @param CsvProductImportVO
   * @param rejectedProducts - List<CsvProductImportVO>
   * @param successProducts - List<CsvProductImportVO>
   *
   */
  private void deleteProductPage(final Resource resource, final CsvProductImportVO productImportVO,
      final List<CsvProductImportVO> rejectedProducts,
      final List<CsvProductImportVO> successProducts) {

    try {
      if (null != resource) {
        Node node = NodeUtils.getNode(resource.getParent().getParent());
        NodeUtils.deleteNode(node);
        productImportVO
            .setStatus("Success: Product deleted with the Gtin:" + productImportVO.getGtin());
        successProducts.add(productImportVO);
      }
    } catch (RepositoryException e) {
      LOGGER.error("RepositoryException in deleteProductPage", e);
      productImportVO.setStatus(
          "Error: In Deleting the existing product with the Gtin:" + productImportVO.getGtin());
      rejectedProducts.add(productImportVO);
    }

  }

  /**
   * method creates the product page
   *
   * @param rootNode - root Node
   * @param CsvProductImportVO
   * @param rejectedProducts - List<CsvProductImportVO>
   * @param successProducts - List<CsvProductImportVO>
   *
   */
  private void createProductPage(final Session session, final CsvProductImportVO productImportVO,
      final List<CsvProductImportVO> rejectedProducts,
      final List<CsvProductImportVO> successProducts) {

    boolean error = false;

    if (!hasValidContent(productImportVO)) {
      LOGGER.debug("Error: Product with Gtin has empty values:" + productImportVO.getGtin());
      productImportVO
          .setStatus("Error: Product with Gtin has empty values:" + productImportVO.getGtin());
      rejectedProducts.add(productImportVO);
      return;
    }

    String productPath = ChoicifyAppConstants.CHOICIFY_PRODUCT_ROOT_PATH
        + productImportVO.getDivison() + "/" + productImportVO.getCountry() + "/"
        + productImportVO.getLanguage() + "/" + productImportVO.getCategory() + "/"
        + productImportVO.getBrandTitle() + "/" + productImportVO.getProductLineTitle();

    Map<String, String> folderProperties = new HashMap<String, String>();
    folderProperties.put(ChoicifyAppConstants.CQ_TEMPLATE_PROPERTY,
        ChoicifyAppConstants.TEMPLATE_FOLDER);
    folderProperties.put(ChoicifyAppConstants.SLING_RESOURCETYPE,
        ChoicifyAppConstants.RESOURCETYPE_FOLDER);
    folderProperties.put(ChoicifyAppConstants.HIDE_IN_NAVIGATION, "true");

    try {
      Node productLine = NodeUtils.createNodeStructureFromPath(productPath, session.getRootNode(),
          ChoicifyAppConstants.CQ_PAGE_NODE, folderProperties);

      // create product page
      if (null != productLine) {
        Map<String, String> productProperties = new HashMap<String, String>();
        productProperties.put(ChoicifyAppConstants.CQ_TEMPLATE_PROPERTY,
            ChoicifyAppConstants.TEMPLATE_PRODUCT);
        productProperties.put(ChoicifyAppConstants.SLING_RESOURCETYPE,
            ChoicifyAppConstants.RESOURCETYPE_PRODUCT);
        productProperties.put(ChoicifyAppConstants.HIDE_IN_NAVIGATION, "true");

        Node productPage = NodeUtils.createNode(productLine, productImportVO.getProductTitle(),
            ChoicifyAppConstants.CQ_PAGE_NODE, productProperties);
        Node jcrNode = NodeUtils.getJcrNode(productPage);

        if (null != jcrNode) {
          Node product = jcrNode.addNode(ChoicifyAppConstants.JCR_NODE_NAME_PRODUCT,
              ChoicifyAppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED);
          product.setProperty(ChoicifyAppConstants.CHOICIFY_GTIN_NAME, productImportVO.getGtin());
          product.setProperty(ChoicifyAppConstants.CHOICIFY_PRODUCT_URL,
              productImportVO.getProductUrl());
          product.setProperty(ChoicifyAppConstants.CHOICIFY_PRODUCT_PAGE_PATH,
              productPage.getPath() + ChoicifyAppConstants.HTML_EXTENSION);
          product.setProperty(ChoicifyAppConstants.JCR_TITLE_NODE,
              productImportVO.getProductTitle());

          // store image path
          Node packshot = product.addNode(ChoicifyAppConstants.JCR_NODE_NAME_PACKSHOT,
              ChoicifyAppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED);
          packshot.setProperty(ChoicifyAppConstants.CHOICIFY_FILE_REFERENCE,
              getProductImagePath(session, productImportVO));

          // set status
          productImportVO
              .setStatus("Success: Created the product with the Gtin:" + productImportVO.getGtin());
          successProducts.add(productImportVO);
        } else {
          error = true;
        }

      } else {
        error = true;
      }

    } catch (RepositoryException e) {
      LOGGER.error("RepositoryException in creatingProductPage", e);
      error = true;
    }

    if (error) {
      LOGGER.debug("Error: In creating the product with the Gtin:" + productImportVO.getGtin());
      productImportVO
          .setStatus("Error: In creating the product with the Gtin:" + productImportVO.getGtin());
      rejectedProducts.add(productImportVO);
    }

  }

  /**
   * If a product data value is empty, return false.
   *
   * @param CsvProductImportVO {@link CsvProductImportVO}
   * @return {@link Boolean}
   */
  private boolean hasValidContent(final CsvProductImportVO productImportVO) {
    if (StringUtils.isEmpty(productImportVO.getGtin())
        || StringUtils.isEmpty(productImportVO.getCountry())
        || StringUtils.isEmpty(productImportVO.getLanguage())
        || StringUtils.isEmpty(productImportVO.getCategory())
        || StringUtils.isEmpty(productImportVO.getBrandTitle())
        || StringUtils.isEmpty(productImportVO.getProductLineTitle())
        || StringUtils.isEmpty(productImportVO.getProductTitle())) {
      return false;
    }
    return true;
  }

  /**
   * method creates the product image and returns image path
   *
   * @param rootNode - root Node
   * @param CsvProductImportVO
   *
   *
   */
  private String getProductImagePath(final Session session,
      final CsvProductImportVO productImportVO) {

    if (StringUtils.isBlank(productImportVO.getImagePath())) {
      LOGGER.debug("Info: Product with Gtin [{}] has no images:" + productImportVO.getGtin());
      return "";
    }

    String imageRootPath =
        ChoicifyAppConstants.CHOICIFY_DAM_ROOT_PATH + "/" + productImportVO.getDivison();

    try {
      Node divison = NodeUtils.createNodeStructureFromPath(imageRootPath, session.getRootNode(),
          ChoicifyAppConstants.SLING_ORDERED_FOLDER, null);

      // create asset
      if (null != divison) {
        // read the image
        URL url = new URL(productImportVO.getImagePath());
        InputStream inputStream = url.openStream();

        if (null != inputStream) {
          String fileType = StringUtils.substringAfterLast(productImportVO.getImagePath(), ".");
          String mimeType = "image/" + fileType;
          String assetPath = divison.getPath() + "/" + productImportVO.getGtin() + "." + fileType;

          // Use AssetManager to create asset into AEM DAM
          AssetManager assetMgr = getThreadVO().getResourceResolver().adaptTo(AssetManager.class);
          Asset asset = assetMgr.createAsset(assetPath, inputStream, mimeType, true);

          // close input stream
          inputStream.close();

          return asset.getPath();
        }
      }
    } catch (IOException e) {
      LOGGER.error("IOException in creatingProductImage", e);
    } catch (RepositoryException e) {
      LOGGER.error("RepositoryException in creatingProductImage", e);
    }

    return "";

  }
}
