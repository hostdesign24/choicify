package com.choicify.core.services;

import java.util.List;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;

public interface ChoicifyAppService {
  /**
   * Search for tagged Product pages with tags from servlet selectors.
   * 
   * @param resourceResolver {@link ResourceResolver}
   * @param selectors {@link List}
   * @return {@link JSONObject}
   * @throws JSONException {@link JSONException}
   */
  public String searchProudctPagesByTags(Session session, final ResourceResolver resourceResolver,
      final List<String> selectors) throws JSONException;

  /**
   * Search natural colors and shades tagged on pages.
   * 
   * @param resourceResolver {@link ResourceResolver}
   * @param session {@link Session}
   * @param selectors {@link List}
   * @return {@link JSONObject}
   * @throws JSONException {@link JSONException}
   * @throws RepositoryException {@link RepositoryException}
   */
  public String searchTargetHairColorTagsOnPages(Session session,
      final ResourceResolver resourceResolver, final List<String> selectors)
      throws JSONException, RepositoryException;

  /**
   * Create JSONObject from natural tags used in the system.
   * 
   * @param resourceResolver {@link ResourceResolver}
   * @return {@link JSONObject}
   * @throws JSONException {@link JSONException}
   */
  public String searchNaturalHairColorTagsOnPages(final ResourceResolver resourceResolver)
      throws JSONException;

  /**
   * Search Product Page By given Property And Value.
   * 
   * @param resourceResolver {@link ResourceResolver}
   * @param propertyName {@link String}
   * @param value {@link String}
   * @return {@link SearchResult}
   */
  public List<Hit> searchProductPageByPropertyAndValue(final ResourceResolver resourceResolver,
      final String propertyName, final String value);

  /**
   * Get Pages From SearchResult.
   * 
   * @param resourceResolver {@link ResourceResolver}
   * @param searchResult {@link List}
   * 
   * @return {@link List} of {@link Page}
   */
  public List<Page> getPagesFromSearchResult(ResourceResolver resourceResolver,
      List<Hit> searchResult);

  /**
   * Get Resources from SearchResult.
   * 
   * @param searchResult {@link SearchResult}
   * @return {@link List} of {@link Resource}
   */
  public List<Resource> getResourcesFromSearchResult(List<Hit> searchResult);

  public Resource getValidTagResource(String tagName, ResourceResolver resourceResolver,
      String rootPath);

  /**
   * Create the value of the tag stored in JCR.
   * 
   * @param nhcTag {@link Tag}
   * @return {@link String}
   */
  public String createTagValueStoredInJcrContent(Tag nhcTag);

  /**
   * Create Path from Selector.
   * 
   * @param selectors {@link List}
   * @return {@link String}
   */
  public String createPathFromSelector(List<String> selectors);



}
