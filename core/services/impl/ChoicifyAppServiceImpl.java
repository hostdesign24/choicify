package com.choicify.core.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.models.ProductModel;
import com.choicify.core.services.ChoicifyAppService;
import com.choicify.core.valueobjects.ColorVO;
import com.choicify.core.valueobjects.ShadeVO;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;

@Service(ChoicifyAppService.class)
@Component(metatype = true, immediate = true, label = "Choicify Product Search Service")
public class ChoicifyAppServiceImpl implements ChoicifyAppService {
  private static final Logger LOGGER = LoggerFactory.getLogger(ChoicifyAppServiceImpl.class);

  private static final String DEFAULT_TAGS_NATURAL_COLORS_PATH =
      "/etc/tags/choicify/haircolor/naturalhair";
  @Property(label = "Default root path of Tags (naturalHairColor)",
      value = DEFAULT_TAGS_NATURAL_COLORS_PATH)
  private static final String TAGS_NATURAL_COLORS_PATH = "tags.natural.colors.path";
  private String naturalHairColorsPath = DEFAULT_TAGS_NATURAL_COLORS_PATH;

  private static final String DEFAULT_PAGES_SEARCH_PATH = "/content/productlib/schwarzkopf/de/de";
  @Property(label = "Default root path of Site (Web pages)", value = DEFAULT_PAGES_SEARCH_PATH)
  private static final String SEARCH_PAGES_PATH = "search.pages.path";
  private String searchPath = DEFAULT_PAGES_SEARCH_PATH;

  private static final String CHOICIFY_PAGES_SEARCH_PATH = "/content/choicifyproducts";
  @Property(label = "Choicify root path of Site (Web pages)", value = "/content/choicifyproducts")
  private static final String CHOICIFY_SEARCH_PAGE_PATH = "choicify.search.page.path";
  private String choicifySearchPath = CHOICIFY_PAGES_SEARCH_PATH;

  private static final String DEFAULT_DOMAIN_NAME = "http://schwarzkopf.de";
  @Property(label = "Default Domain name (http://schwarzkopf.de)", value = DEFAULT_DOMAIN_NAME)
  private static final String CHOICIFY_DOMAIN_NAME = "choicify.domain.name";
  private String choicifyDomainName = DEFAULT_DOMAIN_NAME;

  @Activate
  @Modified
  public void activate(final ComponentContext context) {
    try {
      this.naturalHairColorsPath = (String) context.getProperties().get(TAGS_NATURAL_COLORS_PATH);
      this.searchPath = (String) context.getProperties().get(SEARCH_PAGES_PATH);
      this.choicifyDomainName = (String) context.getProperties().get(CHOICIFY_DOMAIN_NAME);
      this.choicifySearchPath = (String) context.getProperties().get(CHOICIFY_SEARCH_PAGE_PATH);
    } catch (Exception ex) {
      LOGGER.error("Error while reading properties", ex);
    }
  }

  public String searchNaturalHairColorTagsOnPages(final ResourceResolver resourceResolver)
      throws JSONException {
    LOGGER.debug("Start processing choicify natural hair colors and shades.");
    HashMap<String, List<ColorVO>> hashMapColors = new HashMap<String, List<ColorVO>>();
    List<ColorVO> colorVOsnhcWithNhs = new ArrayList<ColorVO>();
    hashMapColors.put(ChoicifyAppConstants.JSON_NAME_NHC_WITH_NHS, colorVOsnhcWithNhs);
    Resource resource = resourceResolver.getResource(this.naturalHairColorsPath);
    if (resource == null) {
      return new Gson().toJson(hashMapColors);
    }
    Tag baseTag = resource.adaptTo(Tag.class);
    if (baseTag == null) {
      return new Gson().toJson(hashMapColors);
    }
    Iterator<Tag> colorTags = baseTag.listChildren();
    while (colorTags.hasNext()) {
      ColorVO colorVO = new ColorVO();
      List<ShadeVO> shadeVOs = new ArrayList<ShadeVO>();
      Tag colorTag = colorTags.next();
      Iterator<Tag> shadeTags = colorTag.listChildren();
      while (shadeTags.hasNext()) {
        ShadeVO shadeVO = new ShadeVO();
        Tag shadeTag = shadeTags.next();
        int tagShadeHitCounter = this.getTagHitCount(resourceResolver,
            ChoicifyAppConstants.NATURAL_HAIR_SHADES_PROP_NAME,
            createTagValueStoredInJcrContent(shadeTag));
        if (tagShadeHitCounter > 0) {
          shadeVO.setNameShade(shadeTag.getName());
          shadeVO.setUrlShade(createImageUrlForColorOrShade(shadeTag,
              ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION, resourceResolver));
          shadeVO.setUrlShadeHead(createImageUrlForWomanHead(shadeTag,
              ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION, resourceResolver));
          shadeVO.setProductsCount(tagShadeHitCounter);
          shadeVOs.add(shadeVO);
        }
      }
      int tagColorHitCounter =
          this.getTagHitCount(resourceResolver, ChoicifyAppConstants.NATURAL_HAIR_COLORS_PROP_NAME,
              createTagValueStoredInJcrContent(colorTag));
      if (tagColorHitCounter > 0) {
        colorVO.setNameColor(colorTag.getName());
        colorVO.setUrlColor(createImageUrlForColorOrShade(colorTag,
            ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION, resourceResolver));
        colorVO.setUrlColorHead(createImageUrlForWomanHead(colorTag,
            ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION, resourceResolver));
        colorVO.setProductsCount(tagColorHitCounter);
        colorVO.setShades(shadeVOs);
      }
      if (StringUtils.isNotEmpty(colorVO.getNameColor())) {
        colorVOsnhcWithNhs.add(colorVO);
      }

    }

    LOGGER.debug("End processing choicify natural hair colors and shades.");
    return new Gson().toJson(hashMapColors);

  }

  /**
   * Create Hair Color or shade asset path from tag path.
   * 
   * @param tag {@link Tag}
   * @param abbreviation {@link String}
   * @param resourceResolver {@link ResourceResolver}
   * @return {@link String}
   */
  private String createImageUrlForColorOrShade(Tag tag, String abbreviation,
      ResourceResolver resourceResolver) {
    String tagPath = tag.getPath();
    String damRootPath = ChoicifyAppConstants.CHOICIFY_DAM_ROOT_PATH;
    String tagRootPath = ChoicifyAppConstants.CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR;
    String extension = ChoicifyAppConstants.CHOCIFY_ASSET_EXTENSION_JPG;
    String assetPath = ChoicifyAppConstants.EMPTY_STRING;
    if (StringUtils.equals(abbreviation, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
        || StringUtils.equals(abbreviation, ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
      assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
          + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + tag.getName() + extension;
    } else {
      String colorFamilyFolderName =
          tag.getName() + ChoicifyAppConstants.CHOCIFY_FOLDER_NAME_FAMILY;
      String assetName =
          tag.getName() + ChoicifyAppConstants.CHOCIFY_FOLDER_NAME_FAMILY + extension;
      assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
          + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + colorFamilyFolderName
          + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + assetName;
    }

    if (resourceResolver.getResource(assetPath) == null) {
      return null;
    }
    return getChoicifyDomainName() + assetPath;
  }

  /**
   * Create image url from tag path.
   * 
   * @param tag {@link Tag}
   * @param abbreviation {@link String}
   * @param resourceResolver {@link ResourceResolver}
   * @return {@link String}
   */
  private String createImageUrlForWomanHead(Tag tag, String abbreviation,
      ResourceResolver resourceResolver) {
    String tagPath = tag.getPath();
    String damRootPath = ChoicifyAppConstants.CHOICIFY_DAM_ROOT_PATH;
    String tagRootPath = ChoicifyAppConstants.CHOICIFY_TAG_ROOT_PATH_HAIR_COLOR;
    String extension = ChoicifyAppConstants.CHOCIFY_ASSET_EXTENSION_PNG;
    String tagNameWithNhs = StringUtils.replace(tag.getName(), abbreviation, "");
    String assetNameWithoutExtension =
        abbreviation + ChoicifyAppConstants.CHOCIFY_IMAGE_WOMAN_HEAD_NAME + tagNameWithNhs;
    String assetPath = "";
    if (StringUtils.equals(abbreviation, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
        || StringUtils.equals(abbreviation, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)) {
      String colorFamilyFolderName =
          tag.getName() + ChoicifyAppConstants.CHOCIFY_FOLDER_NAME_FAMILY;
      assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
          + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + colorFamilyFolderName
          + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + assetNameWithoutExtension + "Family"
          + extension;
    } else {
      assetPath = StringUtils.replace(tagPath, tagRootPath, damRootPath)
          + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + assetNameWithoutExtension + extension;
    }

    if (resourceResolver.getResource(assetPath) == null) {
      return null;
    }

    return getChoicifyDomainName() + assetPath;
  }

  public String createTagValueStoredInJcrContent(Tag tag) {
    String namespacePath = tag.getNamespace().getPath();
    String tagPath = tag.getPath();
    String namespaceName = tag.getNamespace().getName();
    return StringUtils.replace(tagPath,
        (namespacePath + ChoicifyAppConstants.STRING_SEPERATOR_SLASH),
        (namespaceName + ChoicifyAppConstants.STRING_SEPERATOR_COLON));
  }

  /**
   * Search Pages by given Tag(Selectors from Serlvelt).
   * 
   * @param resourceResolver {@link ResourceResolver}
   * @param selectors {@link List}
   * @return {@link List} of {@link Page}
   */
  public List<Page> searchPagesByTags(Session session, final ResourceResolver resourceResolver,
      final List<String> selectors) {
    List<Hit> searchResult = this.searchResults(session, resourceResolver, selectors);

    return getPagesFromSearchResult(resourceResolver, searchResult);
  }

  public List<Page> getPagesFromSearchResult(final ResourceResolver resourceResolver,
      List<Hit> searchResult) {
    List<Page> result = new ArrayList<Page>();
    PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
    for (Hit hit : searchResult) {
      try {
        result.add(pageManager.getContainingPage(hit.getResource()));
      } catch (RepositoryException e) {
        LOGGER.warn("while parsing results: {}", e.getMessage());
      }
    }
    return result;
  }

  public String searchTargetHairColorTagsOnPages(Session session,
      final ResourceResolver resourceResolver, final List<String> selectors)
      throws JSONException, RepositoryException {
    LOGGER.debug("Start processing choicify target hair colors and shades.");
    QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
    TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
    ColorVO naturalColorVO = new ColorVO();
    ShadeVO naturalHairShadeVO = new ShadeVO();
    List<ShadeVO> nhShades = new ArrayList<ShadeVO>();
    String naturalHairColor = null;
    String naturalHairShade = null;
    String tagPath = null;
    if (selectors != null) {
      for (String selector : selectors) {
        if (StringUtils.equals(ChoicifyAppConstants.CHOICIFY_SERVLET_SELECTOR, selector)
            || StringUtils.equals(ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR,
                selector)) {
          continue;
        }
        if (StringUtils.isEmpty(tagPath)) {
          tagPath = getValidTag(selector, resourceResolver, this.naturalHairColorsPath);
        } else {
          Resource resource = resourceResolver.getResource(tagPath).getChild(selector);
          if (resource != null) {
            tagPath = resource.getPath();
          }
        }
        if (selector.startsWith(ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
            && StringUtils.isNotEmpty(tagPath)) {
          naturalColorVO.setNameColor(selector);
          Tag shadeTag = resourceResolver.getResource(tagPath).adaptTo(Tag.class);
          naturalColorVO.setUrlColor(createImageUrlForColorOrShade(shadeTag,
              ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION, resourceResolver));
          naturalColorVO.setUrlColorHead(createImageUrlForWomanHead(shadeTag,
              ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION, resourceResolver));
          naturalHairColor = tagPath;
          continue;
        }
        if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
            && StringUtils.isNotEmpty(naturalColorVO.getNameColor())) {
          naturalHairShadeVO.setNameShade(selector);
          Tag shadeTag = resourceResolver.getResource(tagPath).adaptTo(Tag.class);
          naturalHairShadeVO.setUrlShade(createImageUrlForColorOrShade(shadeTag,
              ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION, resourceResolver));
          naturalHairShadeVO.setUrlShadeHead(createImageUrlForWomanHead(shadeTag,
              ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION, resourceResolver));
          nhShades.add(naturalHairShadeVO);
          naturalHairShade = tagPath;
        }
      }
    }
    HashMap<String, List<ColorVO>> globalHashMapColorsAndShades =
        new HashMap<String, List<ColorVO>>();
    List<ColorVO> colorVOsnhcWithNhs = new ArrayList<ColorVO>();
    globalHashMapColorsAndShades.put("nhcWithNhs", colorVOsnhcWithNhs);
    naturalColorVO.setShades(nhShades);
    colorVOsnhcWithNhs.add(naturalColorVO);

    if (StringUtils.isEmpty(naturalHairColor)) {
      return new Gson().toJson(globalHashMapColorsAndShades);
    }
    List<ColorVO> colorVOsThcWithThs = new ArrayList<ColorVO>();
    globalHashMapColorsAndShades.put("thcWithThs", colorVOsThcWithThs);
    List<Hit> searchResult = searchNodesWithNaturalHairColorOrShades(session, queryBuilder,
        tagManager, naturalHairColor, naturalHairShade);
    naturalColorVO.setProductsCount(searchResult.size());
    naturalHairShadeVO.setProductsCount(searchResult.size());

    return searchTargetColorsFromNaturalColors(session, resourceResolver, naturalColorVO,
        naturalHairShadeVO, globalHashMapColorsAndShades, colorVOsThcWithThs, searchResult);
  }

  /**
   * Search Target colors from given Natuaral Hair Colors.
   * 
   * @param resourceResolver
   * @param naturalColorVO
   * @param naturalHairShadeVO
   * @param globalHashMapColorsAndShades
   * @param colorVOsThcWithThs
   * @param searchResults
   * @return {@link String}
   * @throws RepositoryException
   */
  private String searchTargetColorsFromNaturalColors(Session session,
      final ResourceResolver resourceResolver, ColorVO naturalColorVO, ShadeVO naturalHairShadeVO,
      HashMap<String, List<ColorVO>> globalHashMapColorsAndShades, List<ColorVO> colorVOsThcWithThs,
      List<Hit> searchResults) throws RepositoryException {
    for (Hit hit : searchResults) {
      List<String> shadesColors = new ArrayList<String>();
      shadesColors.add(naturalColorVO.getNameColor());
      shadesColors.add(naturalHairShadeVO.getNameShade());
      Resource resource = hit.getResource();
      ValueMap valueMap = resource.adaptTo(ValueMap.class);
      String[] targetHairColors =
          valueMap.get(ChoicifyAppConstants.TARGET_HAIR_COLORS_PROP_NAME, new String[0]);
      String[] targetHairShades =
          valueMap.get(ChoicifyAppConstants.TARGET_HAIR_SHADES_PROP_NAME, new String[0]);
      ColorVO targetColorVO = null;
      if (targetHairColors != null && targetHairColors.length > 0) {
        Tag tagTargetColor =
            createTagFromTagJcrPropertyValue(targetHairColors[0], resourceResolver);
        if (tagTargetColor != null) {
          targetColorVO = getOrCreateColorVO(tagTargetColor, colorVOsThcWithThs,
              ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION, resourceResolver);
          shadesColors.add(tagTargetColor.getName());
          List<Hit> result = searchResults(session, resourceResolver, shadesColors);
          targetColorVO.setProductsCount(result.size());
        }
      }
      if (targetColorVO != null && targetHairShades != null && targetHairShades.length > 0) {
        Tag tagTargetShade =
            createTagFromTagJcrPropertyValue(targetHairShades[0], resourceResolver);
        if (tagTargetShade != null && StringUtils.equals(tagTargetShade.getParent().getName(),
            targetColorVO.getNameColor())) {
          ShadeVO shadeVO = getOrCreateShadeVOs(tagTargetShade, targetColorVO,
              ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION, resourceResolver);
          shadesColors.add(tagTargetShade.getName());
          List<Hit> result = searchResults(session, resourceResolver, shadesColors);
          shadeVO.setProductsCount(result.size());
        }
      }
    }
    LOGGER.debug("End processing choicify target hair colors and shades.");
    return new Gson().toJson(globalHashMapColorsAndShades);
  }

  /**
   * Create New shade or get existing shade from color list.
   * 
   * @param tagShade {@link Tag}
   * @param colorVO {@link ColorVO}
   * @param abbreviation {@link String}
   * @param resourceResolver {@link ResourceResolver}
   * @return {@link ShadeVO}
   */
  private ShadeVO getOrCreateShadeVOs(Tag tagShade, ColorVO colorVO, String abbreviation,
      ResourceResolver resourceResolver) {
    for (ShadeVO shadeVO : colorVO.getShades()) {
      if (StringUtils.equals(shadeVO.getNameShade(), tagShade.getName())) {
        return shadeVO;
      }
    }
    ShadeVO shadeVO = new ShadeVO();
    shadeVO.setNameShade(tagShade.getName());
    shadeVO.setUrlShade(createImageUrlForColorOrShade(tagShade, abbreviation, resourceResolver));
    shadeVO.setUrlShadeHead(createImageUrlForWomanHead(tagShade, abbreviation, resourceResolver));
    colorVO.getShades().add(shadeVO);
    return shadeVO;
  }

  /**
   * Create Tag path from tag jcr property value.
   * 
   * @param jcrTagValue {@link String}
   * @param resourceResolver {@link ResourceResolver}
   * @return {@link Tag}
   */
  private Tag createTagFromTagJcrPropertyValue(String jcrTagValue,
      ResourceResolver resourceResolver) {
    String namespacePath = ChoicifyAppConstants.CHOICIFY_TAG_ROOT_PATH_NAMESPACE
        + ChoicifyAppConstants.STRING_SEPERATOR_SLASH;
    String tagPath = StringUtils.replace(jcrTagValue, "choicify:", namespacePath);
    Resource tagResource = resourceResolver.getResource(tagPath);
    if (tagResource == null) {
      return null;
    }

    return tagResource.adaptTo(Tag.class);
  }

  /**
   * Get or create Color Value Object with the given tag.
   * 
   * @param tag {@link String}
   * @param colorVOsThcWithThs {@link List}
   * @param abbreviation {@link String}
   * @param resourceResolver {@link ResourceResolver}
   * @return {@link ColorVO}
   */
  private ColorVO getOrCreateColorVO(Tag tag, List<ColorVO> colorVOsThcWithThs, String abbreviation,
      ResourceResolver resourceResolver) {
    for (ColorVO colorVO : colorVOsThcWithThs) {
      if (StringUtils.equals(colorVO.getNameColor(), tag.getName())) {
        return colorVO;
      }
    }
    ColorVO colorVO = new ColorVO();
    colorVO.setNameColor(tag.getName());
    colorVO.setUrlColor(createImageUrlForColorOrShade(tag, abbreviation, resourceResolver));
    colorVO.setUrlColorHead(createImageUrlForWomanHead(tag, abbreviation, resourceResolver));
    colorVO.setShades(new ArrayList<ShadeVO>());
    colorVOsThcWithThs.add(colorVO);
    return colorVO;
  }

  /**
   * Search Nodes with natural hair colors or shades.
   * 
   * @param session {@link Session}
   * @param queryBuilder {@link QueryBuilder}
   * @param tagManager {@link TagManager}
   * @param naturalHairColor {@link String}
   * @param naturalHairShade {@link String}
   * @return {@link SearchResult}
   */
  private List<Hit> searchNodesWithNaturalHairColorOrShades(Session session,
      QueryBuilder queryBuilder, TagManager tagManager, String naturalHairColor,
      String naturalHairShade) {
    List<Hit> results = new ArrayList<Hit>();
    results.addAll(searchNaturalColorsOrShades(session, queryBuilder, tagManager, naturalHairColor,
        naturalHairShade, this.searchPath).getHits());
    results.addAll(searchNaturalColorsOrShades(session, queryBuilder, tagManager, naturalHairColor,
        naturalHairShade, this.choicifySearchPath).getHits());
    return results;
  }

  private SearchResult searchNaturalColorsOrShades(Session session, QueryBuilder queryBuilder,
      TagManager tagManager, String naturalHairColor, String naturalHairShade, String searchPath) {
    Map<String, String> predicateMap = new HashMap<String, String>();
    predicateMap.put(ChoicifyAppConstants.QUERY_STRING_PATH_NAME, searchPath);
    predicateMap.put(ChoicifyAppConstants.QUERY_STRING_TYPE_NAME,
        ChoicifyAppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED);
    predicateMap.put(ChoicifyAppConstants.QUERY_STRING_LIMIT_NAME, "-1");
    if (StringUtils.isNotEmpty(naturalHairColor)) {
      naturalHairColor = createTagValueStoredInJcrContent(tagManager.resolve(naturalHairColor));
      predicateMap.put("1_property", ChoicifyAppConstants.NATURAL_HAIR_COLORS_PROP_NAME);
      predicateMap.put("1_property.value", naturalHairColor);
    }
    if (StringUtils.isNotEmpty(naturalHairShade)) {
      naturalHairShade = createTagValueStoredInJcrContent(tagManager.resolve(naturalHairShade));
      predicateMap.put("2_property", ChoicifyAppConstants.NATURAL_HAIR_SHADES_PROP_NAME);
      predicateMap.put("2_property.value", naturalHairShade);
    }
    PredicateGroup predicateGroup = PredicateGroup.create(predicateMap);
    com.day.cq.search.Query query = queryBuilder.createQuery(predicateGroup, session);
    SearchResult searchResult = query.getResult();
    return searchResult;
  }

  /**
   * Check if tag exist in tagging pool.
   * 
   * @param tagName {@link String}
   * @param resourceResolver {@link ResourceResolver}
   * @param rootPath {@link String}
   * @return {@link String}
   */
  private String getValidTag(String tagName, ResourceResolver resourceResolver, String rootPath) {
    Resource resource = getValidTagResource(tagName, resourceResolver, rootPath);
    if (resource == null) {
      return null;
    }
    return resource.getPath();
  }

  public Resource getValidTagResource(String tagName, ResourceResolver resourceResolver,
      String rootPath) {
    Resource resource = resourceResolver.getResource(rootPath);
    if (resource == null) {
      return null;
    }
    Iterator<Resource> tagColors = resource.listChildren();
    while (tagColors.hasNext()) {
      Resource tagColor = (Resource) tagColors.next();
      if (StringUtils.equals(tagColor.getName(), tagName)) {
        return tagColor;
      }
      Iterator<Resource> tagShades = tagColor.listChildren();
      while (tagShades.hasNext()) {
        Resource tagShade = (Resource) tagShades.next();
        if (StringUtils.equals(tagShade.getName(), tagName)) {
          return tagShade;
        }
      }

    }
    return null;
  }

  /**
   * Search page for a given property name and value. True if any page is found.
   * 
   * @param resourceResolver {@link ResourceResolver}
   * @param propertyName {@link String}
   * @param tagValue {@link String}
   * @return {@link Boolean}
   */
  public int getTagHitCount(final ResourceResolver resourceResolver, final String propertyName,
      final String tagValue) {
    return searchProductPageByPropertyAndValue(resourceResolver, propertyName, tagValue).size();
  }

  public List<Hit> searchProductPageByPropertyAndValue(final ResourceResolver resourceResolver,
      final String propertyName, final String tagValue) {
    Session session = resourceResolver.adaptTo(Session.class);
    QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
    List<Hit> hits = new ArrayList<Hit>();
    hits.addAll(
        searchTagInNode(propertyName, tagValue, session, queryBuilder, this.searchPath).getHits());
    hits.addAll(
        searchTagInNode(propertyName, tagValue, session, queryBuilder, this.choicifySearchPath)
            .getHits());
    return hits;
  }

  private SearchResult searchTagInNode(String propertyName, final String tagValue, Session session,
      QueryBuilder queryBuilder, String searchPath) {
    Map<String, String> predicateMap = new HashMap<String, String>();
    predicateMap.put(ChoicifyAppConstants.QUERY_STRING_PATH_NAME, searchPath);
    predicateMap.put(ChoicifyAppConstants.QUERY_STRING_TYPE_NAME,
        ChoicifyAppConstants.JCR_PRIMARY_TYPE_UNSTRUCTURED);
    predicateMap.put(ChoicifyAppConstants.QUERY_STRING_LIMIT_NAME, "-1");
    predicateMap.put("group.1_property", propertyName);
    predicateMap.put("group.1_property.value", tagValue);
    PredicateGroup predicateGroup = PredicateGroup.create(predicateMap);
    Query query = queryBuilder.createQuery(predicateGroup, session);
    SearchResult searchResult = query.getResult();
    return searchResult;
  }

  public String searchProudctPagesByTags(Session session, final ResourceResolver resourceResolver,
      final List<String> selectors) throws JSONException {
    LOGGER.debug("Start processing choicify product search.");
    JSONObject global = new JSONObject();
    JSONArray filters = new JSONArray();
    JSONArray products = new JSONArray();
    global.put(ChoicifyAppConstants.JSON_NAME_PRODUCT_FILTER, filters);
    global.put(ChoicifyAppConstants.JSON_NAME_PRODUCTS, products);

    TagManager tagManger = resourceResolver.adaptTo(TagManager.class);
    List<Page> pages = this.searchPagesByTags(session, resourceResolver, selectors);
    Map<String, JSONArray> keyfilters = new HashMap<String, JSONArray>();

    if (pages != null && !pages.isEmpty()) {
      createProductJsonOutPut(products, tagManger, pages, keyfilters, resourceResolver);
    }

    if (!keyfilters.isEmpty()) {
      for (Map.Entry<String, JSONArray> entry : keyfilters.entrySet()) {
        String value = entry.getKey();
        JSONObject filterObject = jsonArrayHasObject(ChoicifyAppConstants.JSON_KEY, value, filters);
        if (filterObject == null) {
          filterObject = new JSONObject();
        }
        String name = null;
        String key = null;
        String tagPath = ChoicifyAppConstants.CHOICIFY_TAG_FILTER_ROOT_PATH
            + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + entry.getKey();
        Resource resource = resourceResolver.getResource(tagPath);
        if (resource != null) {
          Tag tag = resource.adaptTo(Tag.class);
          name = tag.getTitle(new Locale("de"));
          key = tag.getName();
        } else {
          name = entry.getKey();
          key = entry.getKey();
        }
        filterObject.put(ChoicifyAppConstants.JSON_NAME, name);
        filterObject.put(ChoicifyAppConstants.JSON_KEY, key);
        filterObject.put(ChoicifyAppConstants.JSON_TYPE, ChoicifyAppConstants.JSON_SELECT);
        filterObject.put(ChoicifyAppConstants.JSON_OPTIONS,
            removeDuplicateFromJSONArray(entry.getValue()));
        filters.put(filterObject);
      }
    }
    LOGGER.debug("End processing choicify product search.");
    return global.toString();
  }

  /**
   * Create json output for products.
   * 
   * @param products {@link JSONArray}
   * @param tagManger {@link TagManager}
   * @param pages {@link List}
   * @param keyfilters {@link HashMap}
   * @param resourceResolver
   * @throws JSONException {@link JSONException}
   */
  private void createProductJsonOutPut(JSONArray products, TagManager tagManger, List<Page> pages,
      Map<String, JSONArray> keyfilters, ResourceResolver resourceResolver) throws JSONException {
    for (Page page : pages) {
      if (!page.isValid()) {
        continue;
      }
      JSONObject productJson = new JSONObject();
      products.put(productJson);
      Resource resource =
          page.getContentResource().getChild(ChoicifyAppConstants.JCR_NODE_NAME_PRODUCT);
      if (resource != null) {
        ProductModel productResource = resource.adaptTo(ProductModel.class);
        if (productResource == null) {
          continue;
        }
        productJson.put(ChoicifyAppConstants.JSON_NAME_BRAND_PAGE_TITLE,
            productResource.getBrandPagetitle());
        productJson.put(ChoicifyAppConstants.JSON_NAME_PRODUCT_PAGE_TITLE,
            productResource.getProductPageTitle());
        productJson.put(ChoicifyAppConstants.JSON_NAME_PRODUCT_TITLE,
            productResource.getProductTitle());
        String absoluteProductPathUrl = productResource.getProductPagePath();
        if (resourceResolver.getResource(absoluteProductPathUrl) != null) {
          absoluteProductPathUrl = getChoicifyDomainName() + productResource.getProductPagePath()
              + ChoicifyAppConstants.HTML_EXTENSION;
        }
        productJson.put(ChoicifyAppConstants.JSON_NAME_PRODUCT_PAGE_URL, absoluteProductPathUrl);
        String absolutePackshotPath =
            getChoicifyDomainName() + productResource.getPackshot().getFileReference();

        productJson.put(ChoicifyAppConstants.JSON_NAME_PRODCUT_IMAGE_URL, absolutePackshotPath);
        productJson.put(ChoicifyAppConstants.JSON_NAME_SUGGESTED_RETAIL_PRICE,
            productResource.getSuggestedRetailsPrice());
        productJson.put(ChoicifyAppConstants.JSON_NAME_SELECTED_STORE_ONLY,
            productResource.getSelectedStoreOnly());

        createFilters(tagManger, keyfilters, productResource);
        createProductTags(tagManger, productJson, productResource, resourceResolver);

      }
    }
  }

  /**
   * Search for an object in json array.
   * 
   * @param key
   * @param value
   * @param filters
   * @return
   * @throws JSONException
   */
  private JSONObject jsonArrayHasObject(String key, String value, JSONArray filters)
      throws JSONException {
    for (int i = 0; i < filters.length(); i++) {
      JSONObject jsonObject = filters.getJSONObject(i);
      if (StringUtils.equals(jsonObject.getString(key), value)) {
        return jsonObject;
      }
    }
    return null;
  }

  /**
   * search json object in json array.
   * 
   * @param key
   * @param filters
   * @return {@link JSONObject}
   * @throws JSONException
   */
  private JSONObject searchObjectInJsonArray(String key, JSONArray filters) throws JSONException {
    for (int i = 0; i < filters.length(); i++) {
      JSONObject jsonObject = filters.getJSONObject(i);
      if (jsonObject.has(key)) {
        return jsonObject;
      }
    }
    return null;
  }

  /**
   * Create search Products filters.
   * 
   * @param tagManger {@link TagManager}
   * @param keyfilters {@link HashMap}
   * @param productResource {@link ProductModel}
   * @throws JSONException {@link JSONException}
   */
  private void createFilters(TagManager tagManger, Map<String, JSONArray> keyfilters,
      ProductModel productResource) throws JSONException {
    Resource filterResource = productResource.getFilterResource();
    if (filterResource != null) {
      Iterator<Resource> resources = filterResource.listChildren();
      while (resources.hasNext()) {
        Resource resource = (Resource) resources.next();
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        for (Map.Entry<String, Object> entry : valueMap.entrySet()) {
          if (ChoicifyAppConstants.JCR_PRIMARY_TYPE_NAME.equals(entry.getKey())) {
            continue;
          }
          if (!keyfilters.containsKey(entry.getKey())) {
            keyfilters.put(entry.getKey(), new JSONArray());
          }
          String[] tagValues = valueMap.get(entry.getKey(), String[].class);
          Tag tag = tagManger.resolve(tagValues[0]);
          JSONArray jSONArray = keyfilters.get(entry.getKey());
          JSONObject jsonObject = new JSONObject();
          if (jsonArrayHasObject(ChoicifyAppConstants.JSON_KEY, tag.getName(), jSONArray) == null) {
            jsonObject.put(ChoicifyAppConstants.JSON_KEY, tag.getName());
            jsonObject.put(ChoicifyAppConstants.JSON_NAME, tag.getTitle(new Locale("de")));
            jSONArray.put(jsonObject);
          }
        }
      }

    }
  }

  /**
   * Create tags for Products.
   * 
   * @param tagManger {@link TagManager}
   * @param productJson {@link JSONObject}
   * @param productResource {@link ProductModel}
   * @param resourceResolver {@link ResourceResolver}
   * @throws JSONException {@link JSONException}
   */
  private void createProductTags(TagManager tagManger, JSONObject productJson,
      ProductModel productResource, ResourceResolver resourceResolver) throws JSONException {
    Map<String, JSONArray> keyTags = new HashMap<String, JSONArray>();
    JSONArray pageTags = new JSONArray();
    productJson.put(ChoicifyAppConstants.JSON_TAGS, pageTags);
    List<Resource> tagResources = productResource.getColorationPaths();
    addTagsToProduct(tagManger, keyTags, tagResources);
    addTagFiltersToProduct(tagManger, productResource, keyTags);
    createTagFiltersforChoicifyApps(keyTags, pageTags, resourceResolver);
  }

  /**
   * Add tags to product.
   * 
   * @param tagManger {@link TagManager}
   * @param keyTags {@link Map}
   * @param tagResources {@link List}
   * @throws JSONException {@link JSONException}
   */
  private void addTagsToProduct(TagManager tagManger, Map<String, JSONArray> keyTags,
      List<Resource> tagResources) throws JSONException {
    if (tagResources != null) {
      for (Resource eachResource : tagResources) {
        if (eachResource != null) {
          ValueMap valueMap = eachResource.adaptTo(ValueMap.class);
          for (Map.Entry<String, Object> entry : valueMap.entrySet()) {
            if (ChoicifyAppConstants.JCR_PRIMARY_TYPE_NAME.equals(entry.getKey())) {
              continue;
            }
            if (!keyTags.containsKey(entry.getKey())) {
              keyTags.put(entry.getKey(), new JSONArray());
            }
            String[] tagValues = valueMap.get(entry.getKey(), String[].class);
            for (String tagString : tagValues) {
              Tag tag = tagManger.resolve(tagString);
              if (tag == null) {
                continue;
              }
              JSONArray jsonArray = keyTags.get(entry.getKey());
              JSONObject tagjson = new JSONObject();
              tagjson.put(ChoicifyAppConstants.JSON_KEY, tag.getName());
              tagjson.put(ChoicifyAppConstants.JSON_NAME, tag.getTitle(new Locale("de")));
              jsonArray.put(tagjson);
            }
          }
        }
      }
    }
  }

  /**
   * create tag used for filtering product on the choicify application.
   * 
   * @param keyTags {@link Map}
   * @param pageTags {@link JSONArray}
   * @param resourceResolver {@link ResourceResolver}
   * @throws JSONException {@link JSONException}
   */
  private void createTagFiltersforChoicifyApps(Map<String, JSONArray> keyTags, JSONArray pageTags,
      ResourceResolver resourceResolver) throws JSONException {
    if (!keyTags.isEmpty()) {
      for (Map.Entry<String, JSONArray> entry : keyTags.entrySet()) {
        JSONObject tagObject = searchObjectInJsonArray(entry.getKey(), pageTags);
        if (tagObject == null) {
          tagObject = new JSONObject();
        }
        String key = null;
        String name = null;
        String tagPath = ChoicifyAppConstants.CHOICIFY_TAG_FILTER_ROOT_PATH
            + ChoicifyAppConstants.STRING_SEPERATOR_SLASH + entry.getKey();
        Resource resource = resourceResolver.getResource(tagPath);
        if (resource != null) {
          Tag tag = resource.adaptTo(Tag.class);
          key = tag.getName();
          name = tag.getTitle(new Locale("de"));
        } else {
          key = entry.getKey();
          name = entry.getKey();
        }
        tagObject.put(ChoicifyAppConstants.JSON_KEY, key);
        tagObject.put(ChoicifyAppConstants.JSON_NAME, name);
        tagObject.put(ChoicifyAppConstants.JSON_OPTIONS,
            removeDuplicateFromJSONArray(entry.getValue()));
        pageTags.put(tagObject);
      }
    }
  }

  /**
   * Remove duplicate json objects
   * 
   * @param jsonArray
   * @return {@link JSONArray}
   * @throws JSONException
   */
  private JSONArray removeDuplicateFromJSONArray(JSONArray jsonArray) throws JSONException {
    Map<String, String> mapJson = new HashMap<String, String>();
    JSONArray jsonArrayFinal = new JSONArray();
    for (int i = 0; i < jsonArray.length(); i++) {
      String key = jsonArray.getJSONObject(i).getString(ChoicifyAppConstants.JSON_KEY);
      String value = jsonArray.getJSONObject(i).getString(ChoicifyAppConstants.JSON_NAME);
      if (mapJson.containsKey(key)) {
        continue;
      } else {
        mapJson.put(key, value);
        JSONObject tagObject = new JSONObject();
        tagObject.put(ChoicifyAppConstants.JSON_KEY, key);
        tagObject.put(ChoicifyAppConstants.JSON_NAME, value);
        jsonArrayFinal.put(tagObject);
      }
    }
    return jsonArrayFinal;
  }

  /**
   * Create and add tag filters to product.
   * 
   * @param tagManger {@link TagManager}
   * @param productResource {@link ProductModel}
   * @param keyTags {@link Map}
   * @throws JSONException {@link JSONException}
   */
  private void addTagFiltersToProduct(TagManager tagManger, ProductModel productResource,
      Map<String, JSONArray> keyTags) throws JSONException {
    Resource resourceFilter = productResource.getFilterResource();
    if (resourceFilter != null) {
      List<Resource> resourceFilters = IteratorUtils.toList(resourceFilter.listChildren());
      for (Resource tagResource : resourceFilters) {
        ValueMap valueMap = tagResource.adaptTo(ValueMap.class);
        if (!keyTags.containsKey(tagResource.getName())) {
          keyTags.put(tagResource.getName(), new JSONArray());
        }
        String[] tagValues = valueMap.get(tagResource.getName(), String[].class);
        if (tagValues != null && tagValues.length > 0) {
          Tag tag = tagManger.resolve(tagValues[0]);
          if (tag == null) {
            continue;
          }
          JSONArray jsonArray = keyTags.get(tagResource.getName());
          JSONObject tagjson = new JSONObject();
          tagjson.put(ChoicifyAppConstants.JSON_KEY, tag.getName());
          tagjson.put(ChoicifyAppConstants.JSON_NAME, tag.getTitle(new Locale("de")));
          jsonArray.put(tagjson);
        }
      }
    }
  }

  private List<Hit> searchResults(Session session, final ResourceResolver resourceResolver,
      final List<String> selectors) {
    if (selectors == null) {
      LOGGER.debug("Empty Selector!");
      return null;
    }
    String naturalHairColor = null;
    String naturalHairShade = null;
    String targetHairColor = null;
    String targetHairShade = null;

    for (String selector : selectors) {
      if (!isSearchTag(selector)) {
        continue;
      }
      Resource tagResource = getValidTagResource(selector, resourceResolver,
          ChoicifyAppConstants.CHOICIFY_TAG_ROOT_PATH_NATURAL_HAIR_COLOR);
      if (tagResource == null) {
        tagResource = getValidTagResource(selector, resourceResolver,
            ChoicifyAppConstants.CHOICIFY_TAG_ROOT_PATH_TARGET_HAIR_COLOR);
      }
      if (tagResource == null) {
        continue;
      }
      if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)) {
        naturalHairColor = createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
      }
      if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)) {
        naturalHairShade = createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
      }
      if (StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)) {
        targetHairColor = createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
      }
      if (StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
        targetHairShade = createTagValueStoredInJcrContent(tagResource.adaptTo(Tag.class));
      }
    }
    if (StringUtils.isEmpty(naturalHairColor)) {
      LOGGER.debug("Natural Hair Color is empty!");
      return null;
    }
    QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
    List<Hit> searchResults = new ArrayList<Hit>();
    searchResults.addAll(searchProducts(this.searchPath, naturalHairColor, naturalHairShade,
        targetHairColor, targetHairShade, session, queryBuilder).getHits());
    searchResults.addAll(searchProducts(this.choicifySearchPath, naturalHairColor, naturalHairShade,
        targetHairColor, targetHairShade, session, queryBuilder).getHits());
    return searchResults;

  }

  private SearchResult searchProducts(final String path, String naturalHairColor,
      String naturalHairShade, String targetHairColor, String targetHairShade, Session session,
      QueryBuilder queryBuilder) {
    Map<String, String> predicateMap = new HashMap<String, String>();
    predicateMap.put("path", path);
    predicateMap.put("type", "nt:unstructured");
    predicateMap.put("p.limit", "-1");
    predicateMap.put("1_property", "naturalHairColor");
    predicateMap.put("1_property.value", naturalHairColor);
    if (StringUtils.isNotEmpty(naturalHairShade)) {
      predicateMap.put("2_property", "naturalHairShade");
      predicateMap.put("2_property.value", naturalHairShade);
    }
    if (StringUtils.isNotEmpty(targetHairColor)) {
      predicateMap.put("3_property", "targetHairColor");
      predicateMap.put("3_property.value", targetHairColor);
    }
    if (StringUtils.isNotEmpty(targetHairShade)) {
      predicateMap.put("4_property", "targetHairShade");
      predicateMap.put("4_property.value", targetHairShade);
    }
    PredicateGroup predicateGroup = PredicateGroup.create(predicateMap);
    com.day.cq.search.Query query = queryBuilder.createQuery(predicateGroup, session);
    return query.getResult();
  }

  /**
   * Validate selectors.
   * 
   * @param selector {@link String}
   * @return {@link Boolean}
   */
  private boolean isSearchTag(String selector) {
    if (StringUtils.equals(ChoicifyAppConstants.CHOICIFY_SERVLET_SELECTOR, selector)
        || StringUtils.equals(ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR,
            selector)
        || StringUtils.equals(ChoicifyAppConstants.JSON_NAME_PRODUCTS, selector) || StringUtils
            .equals(ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR, selector)) {
      return false;
    }
    return true;
  }

  public String getChoicifyDomainName() {
    return choicifyDomainName;
  }

  public List<Resource> getResourcesFromSearchResult(List<Hit> searchResult) {
    List<Resource> result = new ArrayList<Resource>();
    for (Hit hit : searchResult) {
      try {
        result.add(hit.getResource());
      } catch (RepositoryException e) {
        LOGGER.warn("while parsing results: {}", e.getMessage());
      }
    }
    return result;
  }

  /**
   * Create path to store results.
   * 
   * @param selectors
   * @return {@link String}
   */
  public String createPathFromSelector(List<String> selectors) {
    String path = "";
    for (String selector : selectors) {
      if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
          || StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
          || StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)
          || StringUtils.startsWith(selector,
              ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
        path = path + "/" + selector;
      }
    }
    return path;
  }

}
