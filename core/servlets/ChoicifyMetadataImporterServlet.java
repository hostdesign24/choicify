package com.choicify.core.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.rmi.ServerException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.jcr.Session;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.threads.ModifiableThreadPoolConfig;
import org.apache.sling.commons.threads.ThreadPool;
import org.apache.sling.commons.threads.ThreadPoolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.services.ChoicifyAppService;
import com.choicify.core.services.ChoicifyReplicationService;
import com.choicify.core.services.runnable.ChoicifyMetadataImporterService;
import com.choicify.core.valueobjects.ThreadVO;

@SlingServlet(resourceTypes = "sling/servlet/default",
    selectors = {"metadataImporter", "deleteMetadata"}, extensions = "json",
    methods = {"GET", "POST"})
public class ChoicifyMetadataImporterServlet extends SlingAllMethodsServlet {

  /**
   * Serial Version UID
   */
  private static final long serialVersionUID = -2407470181172575579L;

  @Reference
  private ResourceResolverFactory resolverFactory;

  @Reference
  private ChoicifyAppService choicifyAppService;

  @Reference
  ChoicifyReplicationService choicifyReplicationService;

  @Reference
  private ThreadPoolManager threadPoolManager;

  private static String DIFF_POOL = "DIFF_POOL";

  /** Default log. */
  protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

  private ResourceResolver resourceResolver;
  private Session session;

  @Override
  protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
      throws ServerException, IOException {
    response.setContentType("text/plain");
    final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
    if (!ServletFileUpload.isMultipartContent(request)) {
      response.setStatus(500);
      LOGGER.debug("Internal server error: is not MultipartContent");
      response.getWriter().println(String.format("Internal server error: is not MultipartContent"));
      response.getWriter().close();
      return;
    }
    try {
      resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
    } catch (LoginException e) {
      LOGGER.error("LoginExceptionE", e);
    }
    session = resourceResolver.adaptTo(Session.class);
    LOGGER.info("START: processing Colors and Shades from Excel file");
    final Map<String, RequestParameter[]> params = request.getRequestParameterMap();
    for (final Map.Entry<String, RequestParameter[]> pairs : params.entrySet()) {
      RequestParameter[] pArr = pairs.getValue();
      RequestParameter param = pArr[0];
      InputStream stream = param.getInputStream();
      if (stream == null) {
        LOGGER.debug("Selectors not valid {}" + selectors);
        response.setStatus(405);
        response.getWriter().println(String.format("File not properly uploaded"));
        response.getWriter().close();
        return;
      }
      boolean deleteAction = false;
      if (selectors.contains("deleteMetadata")) {
        deleteAction = true;
      }
      ThreadVO threadVO = new ThreadVO();
      threadVO.setDeleteAction(deleteAction);
      threadVO.setResourceResolver(resourceResolver);
      threadVO.setSession(session);
      threadVO.setStream(stream);
      threadVO.setChoicifyAppService(choicifyAppService);
      threadVO.setChoicifyReplicationService(choicifyReplicationService);
      ChoicifyMetadataImporterService choicifyMetadataImporter =
          new ChoicifyMetadataImporterService();
      choicifyMetadataImporter.setThreadVO(threadVO);
      ThreadPool pool = threadPoolManager.get(DIFF_POOL);
      if (pool == null) {
        ModifiableThreadPoolConfig threadPoolConfig = new ModifiableThreadPoolConfig();
        pool = threadPoolManager.create(threadPoolConfig, DIFF_POOL);
      }
      pool.execute(choicifyMetadataImporter);
    }
    LOGGER.info("FINISHED: processing Colors and Shades from Excel file");
    response.setStatus(200);
    response.getWriter().println(String.format(
        "CSV Importer is started. Please reload this page after some seconds to see the results"));
    response.getWriter().close();
  }

}
