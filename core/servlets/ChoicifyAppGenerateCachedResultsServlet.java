package com.choicify.core.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.jcr.Session;
import javax.servlet.ServletException;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.threads.ModifiableThreadPoolConfig;
import org.apache.sling.commons.threads.ThreadPool;
import org.apache.sling.commons.threads.ThreadPoolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.services.ChoicifyAppService;
import com.choicify.core.services.runnable.ChoicifyAppGenerateCachedResultsService;
import com.choicify.core.valueobjects.ThreadVO;

/**
 * Choicify Product Search Servlet. This Servlet searched product pages tagged with choicify colors
 * and shades. Request comes from Choicify Application and Response is json object with product
 * parameters and filter.
 * 
 * @author christophernde
 *
 */
@SlingServlet(resourceTypes = "sling/servlet/default", selectors = {"generateCachedResults"},
    extensions = "json", methods = {"GET", "POST"})
public class ChoicifyAppGenerateCachedResultsServlet extends SlingAllMethodsServlet {
  /**
   * Serial Version UID.
   */
  private static final long serialVersionUID = 7088730893111333649L;
  private static final Logger LOGGER =
      LoggerFactory.getLogger(ChoicifyAppGenerateCachedResultsServlet.class);
  @Reference
  private ResourceResolverFactory resolverFactory;

  @Reference
  private ChoicifyAppService choicifyAppService;

  @Reference
  private ThreadPoolManager threadPoolManager;
  private static String DIFF_POOL = "DIFF_POOL";


  @Override
  protected void doGet(final SlingHttpServletRequest request,
      final SlingHttpServletResponse response) throws ServletException, IOException {
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    JSONObject jsonObject = new JSONObject();
    ResourceResolver resourceResolver = null;
    Session session = null;
    String results = "";
    final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
    if (!selectors.contains(ChoicifyAppConstants.SELECTOR_GENERATE_CACHED_RESULTS)) {
      LOGGER.debug("Selectors not valid {}" + selectors);
      response.setStatus(405);// Method not allowed
      response.getWriter().print(jsonObject);
      return;
    }
    try {
      resourceResolver = this.resolverFactory.getAdministrativeResourceResolver(null);
      session = resourceResolver.adaptTo(Session.class);
      ThreadVO threadVO = new ThreadVO();
      threadVO.setResourceResolver(resourceResolver);
      threadVO.setSession(session);
      threadVO.setChoicifyAppService(choicifyAppService);
      ChoicifyAppGenerateCachedResultsService cachedResultsService =
          new ChoicifyAppGenerateCachedResultsService();
      cachedResultsService.setThreadVO(threadVO);
      ThreadPool pool = threadPoolManager.get(DIFF_POOL);
      if (pool == null) {
        ModifiableThreadPoolConfig threadPoolConfig = new ModifiableThreadPoolConfig();
        pool = threadPoolManager.create(threadPoolConfig, DIFF_POOL);
      }
      pool.execute(cachedResultsService);
    } catch (LoginException e) {
      response.setStatus(500); // Internal server error
      LOGGER.error("LoginException while processing jcr", e);
    } finally {
      if (resourceResolver != null && resourceResolver.isLive()) {
        resourceResolver.close();
      }
      if (session != null && session.isLive()) {
        session.logout();
      }
    }
    response.getWriter().print(results);
  }
}
