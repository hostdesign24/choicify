package com.choicify.core.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;

/**
 * Choicify Product Search Servlet. This Servlet searched product pages tagged with choicify colors
 * and shades. Request comes from Choicify Application and Response is json object with product
 * parameters and filter.
 * 
 * @author christophernde
 *
 */
@SlingServlet(resourceTypes = "sling/servlet/default", selectors = {"cleanCachedResults"},
    extensions = "json", methods = {"GET"})
public class ChoicifyAppCleanCachesServlet extends SlingAllMethodsServlet {
  /**
   * Serial Version UID.
   */
  private static final long serialVersionUID = -1606732307660529980L;


  private static final Logger LOGGER = LoggerFactory.getLogger(ChoicifyAppCleanCachesServlet.class);


  @Reference
  private ResourceResolverFactory resolverFactory;

  @Override
  protected void doGet(final SlingHttpServletRequest request,
      final SlingHttpServletResponse response) throws ServletException, IOException {
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    ResourceResolver resourceResolver = null;
    Session session = null;
    final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
    try {
      resourceResolver = this.resolverFactory.getAdministrativeResourceResolver(null);
      session = resourceResolver.adaptTo(Session.class);
      if (selectors.contains(ChoicifyAppConstants.SELECTOR_CLEAN_CACHES)) {
        LOGGER.debug("Cleaning Cached Results.");
        Resource resource =
            resourceResolver.getResource(ChoicifyAppConstants.CHOICIFY_CACHED_RESULT_ROOT_PATH);
        if (resource != null) {
          resource.adaptTo(Node.class).remove();
          session.save();
        }
        response.getWriter().print("Cached Results cleaned");
        return;
      } else {
        LOGGER.debug("Method not allowed");
        response.setStatus(405);
        response.getWriter().print("Method not allowed");
        return;
      }
    } catch (LoginException e) {
      response.setStatus(500); // Internal server error
    } catch (RepositoryException e) {
      LOGGER.error("RepositoryException while processing jcr", e);
    } finally {
      if (resourceResolver != null && resourceResolver.isLive()) {
        resourceResolver.close();
      }
      if (session != null && session.isLive()) {
        session.logout();
      }
    }
    response.getWriter().print("Cached Results not cleaned");
  }
}
