package com.choicify.core.servlets;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.services.ChoicifyAppService;

/**
 * Choicify Product Search Servlet. This Servlet searched product pages tagged with choicify colors
 * and shades. Request comes from Choicify Application and Response is json object with product
 * parameters and filter.
 * 
 * @author christophernde
 *
 */
@SlingServlet(resourceTypes = "sling/servlet/default", selectors = {"choicify"},
    extensions = "json", methods = {"GET", "POST"})
public class ChoicifyAppServlet extends SlingAllMethodsServlet {
  private static final Logger LOGGER = LoggerFactory.getLogger(ChoicifyAppServlet.class);

  private static final long serialVersionUID = 1L;

  @Reference
  private ResourceResolverFactory resolverFactory;

  @Reference
  private ChoicifyAppService choicifyAppService;

  @Override
  protected void doGet(final SlingHttpServletRequest request,
      final SlingHttpServletResponse response) throws ServletException, IOException {
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");
    JSONObject jsonObject = new JSONObject();
    handleCORS(request, response);
    ResourceResolver resourceResolver = null;
    Session session = null;
    String results = "";
    final List<String> selectors = Arrays.asList(request.getRequestPathInfo().getSelectors());
    if (!isAcceptedSelectors(selectors)) {
      LOGGER.debug("Selectors not valid {}" + selectors);
      response.setStatus(405);// Method not allowed
      response.getWriter().print(jsonObject);
      return;
    }
    try {
      resourceResolver = this.resolverFactory.getAdministrativeResourceResolver(null);
      session = resourceResolver.adaptTo(Session.class);
      String path = "";
      if (selectors.contains(ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR)
          && selectors.size() >= 2) {
        path = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
            + ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR;
        results = getStoreSearchResults(ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, path,
            resourceResolver);
        if (StringUtils.isEmpty(results)) {
          results = this.choicifyAppService.searchNaturalHairColorTagsOnPages(resourceResolver);
        }
      } else if (selectors.contains(ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR)
          && selectors.size() >= 2) {
        path = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
            + ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR
            + createPathFromSelector(selectors);
        results = getStoreSearchResults(ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, path,
            resourceResolver);
        if (StringUtils.isEmpty(results)) {
          results = this.choicifyAppService.searchTargetHairColorTagsOnPages(session,
              resourceResolver, selectors);
        }
      } else if (selectors.contains(ChoicifyAppConstants.PRODUCTS_SELECTOR)
          && selectors.size() > 2) {
        path = ChoicifyAppConstants.STORED_RESULTS_ROOT_PATH
            + ChoicifyAppConstants.PRODUCTS_SELECTOR + createPathFromSelector(selectors);
        results = getStoreSearchResults(ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, path,
            resourceResolver);
        if (StringUtils.isEmpty(results)) {
          results = this.choicifyAppService.searchProudctPagesByTags(session, resourceResolver,
              selectors);
        }
      }
      if (StringUtils.isNotEmpty(results)) {
        storeSearchResults(results, ChoicifyAppConstants.STORED_RESULTS_PROP_NAME_RESULTS, path,
            session);
      }
      session.save();
    } catch (JSONException ex) {
      LOGGER.error("JSONException while", ex);
    } catch (LoginException e) {
      response.setStatus(500); // Internal server error
    } catch (RepositoryException e) {
      LOGGER.error("RepositoryException while processing jcr", e);
    } finally {
      if (resourceResolver != null && resourceResolver.isLive()) {
        resourceResolver.close();
      }
      if (session != null && session.isLive()) {
        session.logout();
      }
    }
    response.getWriter().print(results);
  }

  /**
   * Store search results temporally to improve performace.
   * 
   * @param result
   * @param name
   * @param path
   * @param resourceResolver
   * @throws RepositoryException
   */
  private void storeSearchResults(String result, String name, String path, Session session)
      throws RepositoryException {
    Node resultNode = JcrUtils.getOrCreateByPath(path, false, "nt:unstructured", "nt:unstructured",
        session, true);
    resultNode.setProperty(name, result);
    session.save();
  }

  /**
   * Get results (json string) from a given stored path.
   * 
   * @param name
   * @param path
   * @param resourceResolver
   * @return {@link String}
   * @throws PathNotFoundException
   * @throws RepositoryException
   */
  private String getStoreSearchResults(String name, String path, ResourceResolver resourceResolver)
      throws PathNotFoundException, RepositoryException {
    Resource resource = resourceResolver.getResource(path);
    if (resource == null) {
      return null;
    }
    Node node = resource.adaptTo(Node.class);
    if (node.hasProperty(name)) {
      return node.getProperty(name).getString();
    }
    node.remove();
    return null;
  }

  /**
   * Handle Cross Origin Resource.
   * 
   * @param request
   * @param response
   */
  private void handleCORS(SlingHttpServletRequest request, SlingHttpServletResponse response) {
    response.setHeader(ChoicifyAppConstants.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
    response.setHeader(ChoicifyAppConstants.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
    response.setHeader(ChoicifyAppConstants.ACCESS_CONTROL_ALLOW_HEADERS, "*");
    response.setHeader(ChoicifyAppConstants.ACCESS_CONTROL_ALLOW_METHODS, "*");
    response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
    response.setHeader("Pragma", "no-cache");
  }

  @Override
  protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
      throws ServletException, IOException {
    this.doPost(request, response);
  }

  @Override
  protected void doOptions(SlingHttpServletRequest request, SlingHttpServletResponse response)
      throws ServletException, IOException {
    handleCORS(request, response);
  }

  /**
   * Check if selector is a valid one to search for product.
   * 
   * @param selector {@link String}
   * @return {@link Boolean}
   */
  private boolean isAcceptedSelectors(List<String> selectors) {
    for (String selector : selectors) {
      boolean validSelector = false;
      if (StringUtils.equals(ChoicifyAppConstants.CHOICIFY_SERVLET_SELECTOR, selector)
          || StringUtils.equals(ChoicifyAppConstants.NATURAL_HAIR_COLORS_AND_SHADES_SELECTOR,
              selector)
          || StringUtils.equals(ChoicifyAppConstants.JSON_NAME_PRODUCTS, selector) || StringUtils
              .equals(ChoicifyAppConstants.TARGET_HAIR_COLORS_AND_SHADES_SELECTOR, selector)) {
        validSelector = true;
      }
      if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
          || StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
          || StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)
          || StringUtils.startsWith(selector,
              ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
        validSelector = true;
      }
      if (!validSelector) {
        return false;
      }
    }
    return true;
  }

  /**
   * Create path to store results.
   * 
   * @param selectors
   * @return {@link String}
   */
  private String createPathFromSelector(List<String> selectors) {
    String path = "";
    for (String selector : selectors) {
      if (StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_COLOR_ABBREVIATION)
          || StringUtils.startsWith(selector, ChoicifyAppConstants.NATURAL_HAIR_SHADE_ABBREVIATION)
          || StringUtils.startsWith(selector, ChoicifyAppConstants.TARGET_HAIR_COLOR_ABBREVIATION)
          || StringUtils.startsWith(selector,
              ChoicifyAppConstants.TARGET_HAIR_SHADE_ABBREVIATION)) {
        path = path + "/" + selector;
      }
    }
    return path;
  }
}
