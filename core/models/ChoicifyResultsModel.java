package com.choicify.core.models;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import com.choicify.core.ChoicifyAppConstants;
import com.choicify.core.valueobjects.CsvImportVO;

/**
 * Model to get Hana and AEM store results in json format. This results are stored by a cron job.
 * 
 * @author NdeCN
 *
 */

@Model(adaptables = Resource.class)
public class ChoicifyResultsModel {

  private Long modificationTime;
  private List<CsvImportVO> failureResults;
  private List<CsvImportVO> successResults;
  private List<CsvImportVO> noProductResults;

  @Self
  private Resource resource;

  @PostConstruct
  public void init() throws JSONException {
    String name = resource.getName();
    String path = ChoicifyAppConstants.CHOICIFY_RESULTS_ROOT_PATH + name;
    Resource varResource = resource.getResourceResolver().getResource(path);
    if (varResource == null) {
      return;
    }
    ValueMap valueMap = varResource.adaptTo(ValueMap.class);
    String results = valueMap.get(ChoicifyAppConstants.PROP_NAME_COLOR_RESULTS, String.class);
    modificationTime = valueMap.get("cq:lastModified", Long.class);
    if (StringUtils.isNotEmpty(results)) {
      successResults = createColorsResults(results, "success");
      failureResults = createColorsResults(results, "failure");
      noProductResults = createColorsResults(results, "noProduct");
    }
  }

  /**
   * Create Hana AEM resuts from stored string.
   * 
   * @param results {@link String}
   * @param key {@link String}
   * @return {@link List}
   * @throws JSONException {@link JSONException}
   */
  private List<CsvImportVO> createColorsResults(String results, String key) throws JSONException {
    List<CsvImportVO> healthCheckVOs = new ArrayList<CsvImportVO>();
    JSONObject jsonObject = new JSONObject(results);
    JSONArray jsonArray = jsonObject.getJSONArray(key);
    for (int i = 0; i < jsonArray.length(); i++) {
      JSONObject jObject = (JSONObject) jsonArray.get(i);
      String path = jObject.getString("pagePath");
      String gtin = jObject.getString("gtin");
      if (StringUtils.isEmpty(gtin) || StringUtils.isEmpty(path)) {
        continue;
      }
      CsvImportVO csvImportVO = new CsvImportVO();
      csvImportVO.setNaturalcHairColor(jObject.getString("naturalHairShade"));
      csvImportVO.setNaturalHairShade(jObject.getString("naturalHairShade"));
      csvImportVO.setTargetHairColor(jObject.getString("targetHairColor"));
      csvImportVO.setTargetHairShade(jObject.getString("targetHairShade"));
      healthCheckVOs.add(csvImportVO);
    }
    return healthCheckVOs;
  }

  public Long getModificationTime() {
    return modificationTime;
  }

  public List<CsvImportVO> getFailureResults() {
    return failureResults;
  }

  public List<CsvImportVO> getSuccessResults() {
    return successResults;
  }

  public List<CsvImportVO> getNoProductResults() {
    return noProductResults;
  }

}
