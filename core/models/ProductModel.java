package com.choicify.core.models;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.Self;
import com.choicify.core.ChoicifyAppConstants;

/**
 * Model for Products.
 * 
 * @author christophernde
 *
 */
@Model(adaptables = Resource.class)
public class ProductModel {
  @Inject
  @Optional
  private String gtin;

  @Inject
  @Optional
  private String productPagePath;

  @Inject
  @Optional
  @Named("jcr:title")
  private String title;

  @Inject
  @Optional
  private String shadeNumber;

  @ChildResource
  @Named("packshot")
  @Optional
  private PackshotModel packshot;

  @Self
  private Resource resource;

  private String brandPagetitle;
  private String productPageTitle;
  private String productTitle;
  private boolean suggestedRetailsPrice;
  private boolean selectedStoreOnly;
  private Resource filterResource;
  private Resource productDetailResource;
  private Resource colorationPathResource;
  private List<Resource> colorationPaths;

  @PostConstruct
  public void postConstruct() {
    init();
  }

  private void init() {
    suggestedRetailsPrice = true;
    selectedStoreOnly = true;
    Resource choicifyResource = resource.getChild("choicify");
    if (choicifyResource != null) {
      productDetailResource = choicifyResource.getChild("productdetails");
      filterResource = choicifyResource.getChild("filters");
      colorationPathResource = choicifyResource.getChild("colorationPaths");
    }
    if (productDetailResource != null) {
      ValueMap valueMap = productDetailResource.adaptTo(ValueMap.class);
      if (valueMap.containsKey("suggestedRetailsPrice")) {
        suggestedRetailsPrice = valueMap.get("suggestedRetailsPrice", Boolean.class);
      }
      if (valueMap.containsKey("selectedStoreOnly")) {
        selectedStoreOnly = valueMap.get("selectedStoreOnly", Boolean.class);
      }
    }
    if (colorationPathResource != null) {
      this.colorationPaths = IteratorUtils.toList(colorationPathResource.listChildren());
    }
    PageModel productPageModel = null;
    Resource productPageResource = this.resource.getParent().getParent().getParent();
    Resource productPageJcrContent = productPageResource.getChild("jcr:content");
    if (productPageJcrContent != null) {
      productPageModel = productPageJcrContent.adaptTo(PageModel.class);
    }
    if (productPageModel != null) {
      productPageTitle = productPageModel.getTitle();
    }
    PageModel brandPageModel = null;
    Resource brandPageResource = this.resource.getParent().getParent().getParent().getParent();
    Resource brandPageJcrContent = brandPageResource.getChild("jcr:content");
    if (brandPageJcrContent != null) {
      brandPageModel = brandPageJcrContent.adaptTo(PageModel.class);
    }
    if (brandPageModel != null) {
      brandPagetitle = brandPageModel.getTitle();
      brandPagetitle = addSchwarzKopfToBrand(brandPagetitle, brandPageResource);
    }
    productTitle = this.title;
  }

  private String addSchwarzKopfToBrand(String brandPagetitle, Resource brandPageResource) {
    if (StringUtils.contains(brandPageResource.getPath(),
        ChoicifyAppConstants.CHOICIFY_PRODUCT_LIB_ROOT_PATH)) {
      brandPagetitle = ChoicifyAppConstants.BRAND_TITLE_SCHWARZKOPF + brandPagetitle;
    }
    return brandPagetitle;
  }

  public PackshotModel getPackshot() {
    return this.packshot;
  }

  public List<Resource> getColorationPaths() {
    return colorationPaths;
  }

  public Resource getFilterResource() {
    return this.filterResource;
  }

  public String getGtin() {
    return this.gtin;
  }

  public String getProductPagePath() {
    return this.productPagePath;
  }

  public String getTitle() {
    return this.title;
  }

  public String getShadeNumber() {
    return this.shadeNumber;
  }

  public String getBrandPagetitle() {
    return brandPagetitle;
  }

  public void setBrandPagetitle(String brandPagetitle) {
    this.brandPagetitle = brandPagetitle;
  }

  public String getProductPageTitle() {
    return productPageTitle;
  }

  public void setProductPageTitle(String productPageTitle) {
    this.productPageTitle = productPageTitle;
  }

  public String getProductTitle() {
    return productTitle;
  }

  public void setProductTitle(String productTitle) {
    this.productTitle = productTitle;
  }

  public boolean getSelectedStoreOnly() {
    return selectedStoreOnly;
  }

  public boolean getSuggestedRetailsPrice() {
    return suggestedRetailsPrice;
  }

}
