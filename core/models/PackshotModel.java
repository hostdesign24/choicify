package com.choicify.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

/**
 * Model for images.
 * 
 * @author christophernde
 *
 */
@Model(adaptables = Resource.class)
public class PackshotModel {
  @Inject
  @Optional
  private String fileReference;

  @PostConstruct
  public void postConstruct() {

  }

  public String getFileReference() {
    return this.fileReference;
  }
}
